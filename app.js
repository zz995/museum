'use strict';

const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const log = require('./libs/log').getLogger(module);
const logAct = require('./libs/log').getLogActions();
const passport = require('passport');
const config = require('./config');
const fs = require('fs');
const models = path.join(__dirname, 'models');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

fs.readdirSync(models)
    .filter(file => ~file.indexOf('.js'))
    .forEach(file => require(path.join(models, file)));

const mongoose = require('mongoose');
const Admin = mongoose.model('Admin');
const rout = require('./routes');
const app = express();
const admin = express();
const flash = require('connect-flash');
const resClientJSON = require('./middleware/resClientJSON');

passport.serializeUser((admin, cb) => cb(null, admin.id));

passport.deserializeUser((id, cb) => Admin.load({criteria: {_id: id}}, cb));

passport.use(require('./passport/local'));

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(express.static(path.join(__dirname, 'public')));

app.set('views', path.join(__dirname, 'templates'));

app.set('view engine', 'jade');

app.use(logger('dev'));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({extended: true}));

app.use(methodOverride('_method'));

admin.use(cookieParser());

admin.use(session({
    resave: true,
    saveUninitialized: true,
    secret: config.get('session:secret'),
    cookie: config.get('session:cookie'),
    store: new MongoStore({mongooseConnection: mongoose.connection})
}));

admin.use(flash());

admin.use(passport.initialize());

admin.use(passport.session());

app.use(/((?!api($|\/)).)*/, admin);

app.use('/api', resClientJSON);

app.use(require('./middleware/sendDataClient'));

app.use('/', function (req, res, next) {
    if (req.originalMethod != 'GET') {
        logAct.info({
            name: req.user ? req.user.adminname : 'undefined',
            method: req.originalMethod,
            url: req.originalUrl
        });
    }
    next();
});

app.use('/', rout);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(404);
});

const HttpError = require('./libs/error').HttpError;

// error handlers
app.use(function (err, req, res, next) {
    if (typeof err === 'number') {
        err = new HttpError(err);
    }
    if (err instanceof HttpError) {
        res.sendDataClient(err);
    } else {
        log.error(err.stack);
        err = new HttpError(500);
        res.sendDataClient(err);
    }
});

module.exports = app;
