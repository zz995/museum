'use strict';

const router = require('express').Router();
const admin = require('../controllers/admin');
const auth = require('../middleware/checkAuth');
const category = require('../controllers/category');
const exhibits = require('../controllers/exhibit');
const museum = require('../controllers/museum');
const tag = require('../controllers/tag');
const message = require('../controllers/message');
const resClientJSON = require('../middleware/resClientJSON');
const imageManager = require('../controllers/imageManager');
const comment = require('../controllers/comment');
const monitoring = require('../controllers/monitoring');
const passport = require('passport');

router.get('/admin/exhibit/image', auth.login, resClientJSON, imageManager.index);
router.delete('/admin/exhibit/image', auth.login, resClientJSON, imageManager.destroy);
router.put('/admin/exhibit/image', auth.login, resClientJSON, imageManager.rename);
router.put('/admin/exhibit/image/move', auth.login, resClientJSON, imageManager.move);
router.post('/admin/exhibit/image/dir', auth.login, resClientJSON, imageManager.dir);
router.post('/admin/exhibit/image', auth.login, resClientJSON, imageManager.imgUpload);

router.param('id', exhibits.load);
router.get('/admin/exhibit', auth.login, exhibits.setOptions, exhibits.index);
router.get('/api', exhibits.setOptions, exhibits.list); //Show list exhibit
router.get('/api/exhibit', exhibits.setOptions, exhibits.list); //Show list exhibit
router.get('/admin/exhibit/new', auth.login, exhibits.new);
router.get('/api/exhibit/:id', exhibits.showforapi); //Show exhibit
router.get('/admin/exhibit/:id/edit', auth.login, exhibits.edit);
router.post('/admin/exhibit', auth.login, exhibits.uploadAndParse, exhibits.create,
    exhibits.resize, exhibits.save);
router.put('/admin/exhibit/:id', auth.login, exhibits.uploadAndParse, exhibits.update,
    exhibits.resize, exhibits.save);
router.delete('/admin/exhibit/:id', auth.login, exhibits.destroy);


router.get('/admin/login', admin.login);
router.get('/admin/logout', auth.login, admin.logout);
router.post('/admin/session',
    passport.authenticate('local', {
        failureRedirect: '/admin/login',
        failureFlash: true
    }), admin.session);
router.get('/admin', auth.login, monitoring.index);
router.get('/', auth.login, monitoring.index);

router.param('ida', admin.load);
router.get('/admin/forgot', admin.forgot);
router.post('/admin/forgot', admin.recovery);
router.get('/admin/reset/:token', admin.reset);
router.post('/admin/reset/:token', admin.changePassword);
router.get('/admin/profile/:ida', auth.checkAccess, admin.profile);
router.put('/admin/profile/:ida', auth.checkAccess, admin.changeProfile);
router.post('/admin/profile/:ida', auth.rootAccess, admin.create);
router.delete('/admin/profile/:ida/:idAdmDel', auth.rootAccess, admin.destroy);

router.get('/admin/message', auth.login, message.index);
router.delete('/admin/message', auth.login, message.destroy);

router.param('idc', category.load);
router.get('/api/category', category.showCategory); //List categories
router.get('/admin/exhibit/category/:idc', auth.login, category.index, exhibits.setOptions, exhibits.index); //Search by category exhibit
router.get('/api/exhibit/category/:idc', category.index, exhibits.setOptions, exhibits.list);
router.get('/api/category/:idc', category.info); //Get more info by category
router.put('/admin/category/:idc', auth.login, //Change category
    resClientJSON, category.uploadAndParse, category.deleteImg,
    category.update, category.resize, category.save
);
router.put('/admin/category/:idc/move', auth.login, //Change category
    resClientJSON, category.move
);
router.delete('/admin/category/:idc', auth.login, resClientJSON, category.destroy); //Delete category
router.post('/admin/category/', auth.login, //Create new category
    resClientJSON, category.uploadAndParse, category.create,
    category.resize, category.save
);

router.get('/admin/museum/:idc', auth.login, museum.edit);
router.put('/admin/museum/:idc', auth.login, museum.update);
router.delete('/admin/museum/:idc', auth.login, museum.destroy);
router.get('/api/museum/:idc', museum.info);

router.get('/admin/tags/:tag', auth.login, tag.index, exhibits.index);  //Find by tag
router.get('/api/tags', tag.list);
router.get('/api/tags/:tag', tag.index, exhibits.list);

router.param('idcom', comment.load);
router.get('/api/comment', comment.list);
router.get('/admin/comment', auth.login, comment.index);
router.post('/api/comment', comment.create);
router.put('/admin/comment/:idcom', auth.login, comment.update);
router.delete('/admin/comment/:idcom', auth.login, comment.destroy);

router.get('/admin/monitoring', auth.login, monitoring.index);

module.exports = router;