(function () {
    function addSortParam() {
        var href = document.getElementById('idButAppendSort').href;
        href = href.replace(/((&|\?)sort=)createdAt_-?1/, '$1' + this.value);
        document.getElementById('idButAppendSort').href = href;
    }

    function addCrtParam() {
        var href = document.getElementById('idButAppendSort').href;
        if (this.value === 'all') {
            href = href.replace(/(&|\?)view=[^&|$]+/, '');
        } else if (this.value === 'viewTrue') {
            if (href.indexOf('&view=') + 1) {
                href = href.replace(/((&|\?)view=)false/, '$1true');
            } else {
                href += '&view=true';
            }
        } else if (this.value === 'viewFalse') {
            if (href.indexOf('&view=') + 1) {
                href = href.replace(/((&|\?)view=)true/, '$1false');
            } else {
                href += '&view=false';
            }
        }
        document.getElementById('idButAppendSort').href = href;
    }

    function addLimitParam() {
        var href = document.getElementById('idButAppendSort').href;
        href = href.replace(/((&|\?)limit=)[0-9]{1,3}/, '$1' + this.value);
        document.getElementById('idButAppendSort').href = href;
    }

    function addViewedDelParam() {
        document.getElementById('delViewed').value = this.value == 'v';
    }

    document.getElementById('sortMsg').onchange = addSortParam;
    document.getElementById('crtMsg').onchange = addCrtParam;
    document.getElementById('limitMsg').onchange = addLimitParam;
    document.getElementById('viewed').onchange = addViewedDelParam;
})();