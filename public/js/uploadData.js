(function () {
    var exc = false;
    var countCharact = 0;
    var countMedia = 0;

    function addChart(that) {
        countCharact = countCharact || that.dataset.ch;
        if (exc) return false;
        exc = true;
        var legend = that.parentNode.parentNode;
        var div = document.createElement('div');
        div.setAttribute("class", "form-inline");
        div.innerHTML = that.parentNode.innerHTML;
        (that.parentNode.lastChild).classList.remove("hidden");
        for (var i = 0, t = that.parentNode.firstChild; i < 2; t = t.nextSibling, i++) {
            t.removeAttribute('id');
        }
        t.previousSibling.setAttribute('name', 'characteristics[' + countCharact + '][value]');
        t.previousSibling.previousSibling.setAttribute('name', 'characteristics[' + countCharact + '][name]');
        legend.appendChild(div);
        exc = false;
        countCharact++;
        return false;
    }

    function removeFormInline(that) {
        countCharact = countCharact || document.getElementById('chName').dataset.ch;
        var m = (that.parentNode.firstChild).getAttribute('name');
        var elem = that.parentNode.parentNode.lastChild.previousSibling;
        for (var i = --countCharact, l = ~~m.slice(m.indexOf('[') + 1, m.indexOf('][')); l < i; i--, elem = elem.previousSibling) {
            var elemFirst = elem.firstChild;
            elemFirst.nextSibling.name = 'characteristics[' + (i - 1) + '][value]';
            elemFirst.name = 'characteristics[' + (i - 1) + '][name]';
        }
        (that.parentNode.parentNode).removeChild(that.parentNode);
        return false;
    }

    function change(that) {
        var next = that.nextSibling;
        if (next.hasAttribute('disabled')) {
            next.removeAttribute('disabled');
            that.previousSibling.setAttribute('type', 'number');
        } else {
            next.setAttribute('disabled', null);
            that.previousSibling.setAttribute('type', 'text');
        }
        return false;
    }

    function addMediaCDN(that) {
        countMedia = countMedia || that.dataset.md;
        var legend = that.parentNode.parentNode;
        var div = document.createElement('div');
        div.setAttribute("class", "form-inline");
        div.innerHTML = that.parentNode.innerHTML;
        (that.parentNode.lastChild).classList.remove("hidden");
        that.removeAttribute("id");

        var elem = that.parentNode.firstChild;
        elem.setAttribute('name', 'mediaCDN[' + countMedia + '][link]');
        elem = elem.nextSibling;
        elem.setAttribute('name', 'mediaCDN[' + countMedia + '][type]');
        elem = elem.nextSibling;
        elem.setAttribute('name', 'mediaCDN[' + countMedia + '][description]');
        legend.appendChild(div);

        countMedia++;
        return false;
    }

    function removeMediaCDN(that) {
        countMedia = countMedia || document.getElementById('media').dataset.md;
        var m = (that.parentNode.firstChild).getAttribute('name');
        var elem = that.parentNode.parentNode.lastChild.previousSibling;
        for (var i = --countMedia, l = ~~m.slice(m.indexOf('[') + 1, m.indexOf('][')); l < i; i--, elem = elem.previousSibling) {
            var elemInput = elem.firstChild;
            elemInput.name = 'mediaCDN[' + (i - 1) + '][link]';
            elemInput = elemInput.nextSibling;
            elemInput.name = 'mediaCDN[' + (i - 1) + '][type]';
            elemInput = elemInput.nextSibling;
            elemInput.name = 'mediaCDN[' + (i - 1) + '][description]';

        }
        (that.parentNode.parentNode).removeChild(that.parentNode);
        return false;
    }

    document.getElementById('idCh').onclick = function (e) {
        var target = e.target;
        for (var t = target; t != this; t = t.parentNode) {
            if (t.id == 'chName' || t.id == 'chValue') {
                return addChart(t);
            }
            if (t.id == 'deleteCh') {
                return removeFormInline(t);
            }
        }

        return false;
    };

    document.getElementById('idMedia').onclick = function (e) {
        var target = e.target;
        for (var t = target; t != this; t = t.parentNode) {
            if (t.id == 'media') {
                return addMediaCDN(t);
            }
            if (t.id == 'deleteMedia') {
                return removeMediaCDN(t);
            }
        }

        return false;
    };

    document.getElementById('clearCat').onclick = function () {
        document.getElementById('nameListCateg').value = '';
        document.getElementById('listCateg').value = '';
        return false;
    };

    var delExhibit = document.getElementById('delExhibit');
    if (delExhibit) {
        delExhibit.onclick = function () {
            var modal = new ConfirmModalWindow();
            var init = {
                head: 'Підтвердження видалення експоната',
                body: 'Видалити?'
            };
            modal.show(init, function (ob) {
                if (ob && ob.yes) document.getElementById('delExhibitForm').submit();
            });
            return false;
        }
    }

    document.addEventListener("DOMContentLoaded", function () {
        document.getElementById('nameListCateg').onfocus = function () {
            document.getElementById('nameListCateg').blur();

            return false;
        };
    });

})();

