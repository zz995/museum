var dragManager = new function () {
    var dragZone, avatar, dropTarget;
    var downX, downY;

    this.move = false;
    this.idDrop = null;
    this.idTarget = null;

    var that = this;

    function onMouseDown(event) {
        if (event.which != 1) return false;
        if (that.move) return false;

        var elem = event.target;

        var cat = document.getElementById('_0');
        for (; elem != document && elem != cat && !elem.dragZone; elem = elem.parentNode) {
        }

        dragZone = elem.dragZone;

        if (!dragZone) return;

        downX = event.pageX;
        downY = event.pageY;

        return false;
    }

    function onMouseMove(event) {
        if (!dragZone) return;

        if (!avatar) {
            if (Math.abs(event.pageX - downX) < 3 && Math.abs(event.pageY - downY) < 3) return;
            avatar = dragZone.onDragStart(downX, downY, event);

            if (!avatar) {
                cleanUp();
                return;
            }
            that.idDrop = event.target.id.slice(1);
            that.move = true;
        }

        avatar.onDragMove(event);

        var elem = avatar.getTargetElem();

        for (; elem != document && !elem.dropTarget; elem = elem.parentNode) {
        }

        if (!elem.dropTarget) return null;

        var newDropTarget = elem.dropTarget;

        if (newDropTarget != dropTarget) {
            dropTarget && dropTarget.onDragLeave(newDropTarget, avatar, event);
        }

        dropTarget = newDropTarget;

        dropTarget && dropTarget.onDragMove(avatar, event);

        return false;
    }

    function onMouseUp(event) {

        if (event.which != 1) return false;

        if (avatar) {

            if (dropTarget) {
                dropTarget.onDragEnd(avatar, event, that);
            } else {
                that.move = false;
                avatar.onDragCancel();
            }

        }

        cleanUp();
    }

    function cleanUp() {
        that.idTarget = that.idDrop = null;
        dragZone = avatar = dropTarget = null;
    }

    document.ondragstart = function () {
        return false;
    };

    document.onmousemove = onMouseMove;
    document.onmouseup = onMouseUp;
    document.onmousedown = onMouseDown;

};

function DragZone(elem) {
    elem.dragZone = this;
    this._elem = elem;
}

DragZone.prototype.onDragStart = function (downX, downY, event) {

    var avatar = new DragAvatar(this, this._elem);

    if (!avatar.initFromEvent(downX, downY, event)) return false;

    return avatar;
};

function DragAvatar(dragZone, dragElem) {
    this._dragZone = dragZone;
    this._dragZoneElem = dragElem;
    this._elem = dragElem;
}

DragAvatar.prototype.getDragInfo = function (event) {
    return {
        elem: this._elem,
        dragZoneElem: this._dragZoneElem,
        dragZone: this._dragZone
    };
};

DragAvatar.prototype.getTargetElem = function () {
    return this._currentTargetElem;
};

DragAvatar.prototype.onDragMove = function (event) {
    this._elem.style.left = event.pageX - this._shiftX + 'px';
    this._elem.style.top = event.pageY - this._shiftY + 'px';

    function getElementUnderClientXY(elem, clientX, clientY) {
        var display = elem.style.display || '';
        elem.style.display = 'none';

        var target = document.elementFromPoint(clientX, clientY);

        elem.style.display = display;

        if (!target || target == document) target = document.body;

        return target;
    }

    this._currentTargetElem = getElementUnderClientXY(this._elem, event.clientX, event.clientY);
};


DragAvatar.prototype.initFromEvent = function (downX, downY, event) {
    if (event.target.tagName != 'SPAN') return false;

    this._dragZoneElem = event.target;
    var elem = this._elem = this._dragZoneElem.cloneNode(true);
    elem.className = 'avatar';


    function getCoords(elem) {
        var box = elem.getBoundingClientRect();

        var body = document.body;
        var docElem = document.documentElement;

        var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
        var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

        var clientTop = docElem.clientTop || body.clientTop || 0;
        var clientLeft = docElem.clientLeft || body.clientLeft || 0;

        var top = box.top + scrollTop - clientTop;
        var left = box.left + scrollLeft - clientLeft;

        return {
            top: Math.round(top),
            left: Math.round(left)
        };
    }


    var coords = getCoords(this._dragZoneElem);
    this._shiftX = downX - coords.left;
    this._shiftY = downY - coords.top;

    document.body.appendChild(elem);
    elem.style.zIndex = 9999;
    elem.style.position = 'absolute';

    return true;
};

DragAvatar.prototype._destroy = function () {
    this._elem.parentNode.removeChild(this._elem);
};

DragAvatar.prototype.onDragCancel = function () {
    this._destroy();
};

DragAvatar.prototype.onDragEnd = function () {
    this._destroy();
};

function DropTarget(elem) {
    elem.dropTarget = this;
    this._elem = elem;
    this._targetElem = null;
}

DropTarget.prototype.onDragMove = function (avatar, event) {
    var newTargetElem = this._getTargetElem(avatar, event);
    if (this._targetElem != newTargetElem) {
        this._hideHoverIndication(avatar);
        this._targetElem = newTargetElem;
        this._showHoverIndication(avatar);
    }
};

DropTarget.prototype.onDragLeave = function (toDropTarget, avatar, event) {
    this._hideHoverIndication();
    this._targetElem = null;
};

DropTarget.prototype._showHoverIndication = function () {
    this._targetElem && this._targetElem.classList.add('hover');
};

DropTarget.prototype._hideHoverIndication = function () {
    this._targetElem && this._targetElem.classList.remove('hover');
};

DropTarget.prototype._getTargetElem = function (avatar, event) {
    var target = avatar.getTargetElem();
    if (target.tagName != 'SPAN' || target.parentNode.tagName == 'A') return;

    var elemToMove = avatar.getDragInfo(event).dragZoneElem.parentNode;

    var elem = target;
    for (; elem; elem = elem.parentNode) {
        if (elem == elemToMove) return;
    }

    return target;
};

DropTarget.prototype.onDragEnd = function (avatar, event, manager) {

    if (!this._targetElem) {
        avatar.onDragCancel();
        manager.move = false;
        return;
    }

    this._hideHoverIndication();

    avatar.onDragEnd();

    this.elemToMove = avatar.getDragInfo(event).dragZoneElem.parentNode;

    manager.idTarget = this._targetElem.id.slice(1);

    var xhr = new XMLHttpRequest();
    var body = 'id=' + encodeURIComponent(manager.idTarget);

    xhr.open('PUT', '/admin/category/' + manager.idDrop + '/move', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    var cb = this.create.bind(this, manager.idDrop);

    xhr.onreadystatechange = function () {
        if (xhr.readyState != 4) return;
        var ans = JSON.parse(xhr.responseText);
        if (xhr.status === 200) {
            (new AlertModalWindow()).show({
                head: 'Переміщення категорії',
                body: ans.message
            });
            manager.move = false;
            cb();
        } else {
            manager.move = false;
            (new ErrorModalWindow()).show({
                head: 'Переміщення категорії',
                body: ans.message
            });
        }
    };

    xhr.send(body);
};

DropTarget.prototype.create = function (id) {
    var elem = null;
    elem = document.getElementById('_' + id);
    elem.parentNode.getElementsByClassName('info')[0].parentNode.hidden = this._targetElem.id !== '_0';
    elem = this._targetElem.parentNode;
    elem.getElementsByClassName('showCat')[0].parentNode.hidden = false;
    elem.getElementsByClassName('delCat')[0].parentNode.hidden = true;

    var ul = this._targetElem.parentNode.getElementsByTagName('UL')[0];
    if (!ul) {
        ul = document.createElement('UL');
        this._targetElem.parentNode.appendChild(ul);
    }

    var oldParent = this.elemToMove.parentNode;

    ul.appendChild(this.elemToMove);

    if (!oldParent.childElementCount) {
        elem = oldParent.parentNode;
        elem.removeChild(oldParent);
        elem.getElementsByClassName('showCat')[0].parentNode.hidden = true;
        elem.getElementsByClassName('delCat')[0].parentNode.hidden = false;
    }

    this.elemToMove = undefined;
    this._targetElem = null;
};