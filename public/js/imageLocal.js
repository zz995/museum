(function () {
    function showListImgView(input) {
        var files = input.files;
        var localImg = document.getElementById('localImg');
        var wrap = localImg.cloneNode(false);
        for (i = 0, l = files.length; i < l; i++) {
            var file = files[i];
            var div = document.createElement('div');
            div.className = 'wrap';

            var p = document.createElement('p');
            p.textContent = files[i].name;

            div.appendChild(p);

            var textarea = document.createElement('textarea');
            textarea.className = 'form-control';
            textarea.placeholder = 'Введіть опис даного зображення';
            textarea.row = 3;
            textarea.name = 'descripImg[' + files[i].name + ']';
            div.appendChild(textarea);

            var img = document.createElement('img');
            img.className = 'thumb';

            div.appendChild(img);
            wrap.appendChild(div);

            var reader = new FileReader();
            reader.onloadend = function (img, reader) {
                return function () {
                    img.src = reader.result;
                }
            }(img, reader);
            reader.readAsDataURL(file);
        }
        localImg.parentNode.replaceChild(wrap, localImg);
    }

    document.getElementById('localImgId').onchange = function () {
        showListImgView(this);
    };
    showListImgView(document.getElementById('localImgId'));
})();