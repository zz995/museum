(function () {
    function addSortParam() {
        var href = document.getElementById('idButAppend').href;
        href = href.replace(/sort=(-|\w)+/, 'sort=' + this.value);
        document.getElementById('idButAppend').href = href;
    }

    function addShowParam() {
        var href = document.getElementById('idButAppend').href;
        if (this.value === 'a') {
            href = href.replace(/&?hidden=\w+/, '');
        } else if (this.value === 'v') {
            if (href.indexOf('hidden=') + 1) {
                href = href.replace(/hidden=\w+/, 'hidden=false');
            } else {
                href += '&hidden=false';
            }
        } else if (this.value === 'h') {
            if (href.indexOf('hidden=') + 1) {
                href = href.replace(/hidden=\w+/, 'hidden=true');
            } else {
                href += '&hidden=true';
            }
        }
        document.getElementById('idButAppend').href = href;
    }

    function addSearchParam(that, str) {
        var href = document.getElementById('idButAppend').href;
        var reg = new RegExp('(&?)' + str + '=[^&]*');
        if (!that.value) {
            href = href.replace(reg, '');
        } else {
            if (href.indexOf(str + '=') + 1) {
                href = href.replace(reg, '$1' + str + '=' + encodeURIComponent(that.value));
            } else {
                href += '&' + str + '=' + encodeURIComponent(that.value);
            }
        }

        document.getElementById('idButAppend').href = href;
    }

    document.getElementById('sortExh').onchange = addSortParam;
    document.getElementById('showExh').onchange = addShowParam;
    document.getElementById('searchExh').onchange = function () {
        addSearchParam(this, 'search');
    };
    document.getElementById('searchExhChname').onchange = function () {
        addSearchParam(this, 'chname');
    };
    document.getElementById('searchExhChvalue').onchange = function () {
        addSearchParam(this, 'chvalue');
    };
})();