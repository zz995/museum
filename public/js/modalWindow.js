var AlertModalWindow = function () {
    var headerWindow = document.createElement('div');
    headerWindow.classList.add('modal-header');

    var close = document.createElement('span');
    close.classList.add('close');
    headerWindow.appendChild(close);
    close.textContent = 'X';
    var headerText = document.createElement('h4');

    headerWindow.appendChild(headerText);

    var bodyWindow = document.createElement('div');
    bodyWindow.classList.add('modal-body');
    var bodyText = document.createElement('p');

    bodyWindow.appendChild(bodyText);

    var window = document.createElement('div');
    window.classList.add('modal-content');
    window.appendChild(headerWindow);
    window.appendChild(bodyWindow);

    var modWindow = document.createElement('div');
    modWindow.classList.add('modal');
    modWindow.setAttribute('id', 'modalWindow');
    modWindow.appendChild(window);

    this.headerWindow = headerWindow;
    this.modWindow = modWindow;
    this.bodyWindow = bodyWindow;
    this.headerText = headerText;
    this.bodyText = bodyText;
    this.close = close;

    this.cb = null;

    this.response = {};
};

AlertModalWindow.prototype._cancel = function () {
    var that = this;
    that.modWindow.style.display = 'none';
    document.body.removeEventListener('click', this.rm, false);
    document.body.removeChild(that.modWindow);
    if (this.cb) {

        this.cb(that.response);
    }
};

AlertModalWindow.prototype._winListener = function (event) {
    if (event.target == this.modWindow) {
        this._cancel();
    }
};

AlertModalWindow.prototype.show = function (init, cb) {
    var modal = this.modWindow;
    var body = document.body;
    this.headerText.textContent = init.head;
    this.bodyText.textContent = init.body;
    this.cb = cb;

    body.appendChild(modal);
    modal.style.display = 'block';

    this.close.onclick = this._cancel.bind(this);
    this.rm = this._winListener.bind(this);
    body.addEventListener('click', this.rm, false);
};

var ConfirmModalWindow = function () {
    AlertModalWindow.apply(this);
    var buttonOk = document.createElement('button');
    this.bodyWindow.appendChild(buttonOk);
    buttonOk.classList.add('btn');
    buttonOk.classList.add('btn-default');
    var buttonCancel = buttonOk.cloneNode(false);
    buttonCancel.classList.add('space');
    buttonOk.textContent = 'Добре';
    buttonCancel.textContent = 'Відміна';

    this.bodyWindow.appendChild(buttonCancel);
    this.bodyWindow.appendChild(buttonOk);

    this.buttonOk = buttonOk;
    this.buttonCancel = buttonCancel;
};

ConfirmModalWindow.prototype = Object.create(AlertModalWindow.prototype);
ConfirmModalWindow.prototype.constructor = ConfirmModalWindow;

ConfirmModalWindow.prototype.show = function (cb) {
    this.buttonCancel.onclick = this._cancel.bind(this);
    var that = this;

    this.buttonOk.onclick = function () {
        that.response.yes = true;
        that._cancel();
    };

    AlertModalWindow.prototype.show.apply(this, arguments);
};

var ErrorModalWindow = function () {
    AlertModalWindow.apply(this);

    this.headerWindow.style.backgroundColor = "red";
};

ErrorModalWindow.prototype = Object.create(AlertModalWindow.prototype);
ErrorModalWindow.prototype.constructor = ErrorModalWindow;

var PromptModalWindow = function () {
    AlertModalWindow.apply(this);
    var input = document.createElement('input');
    input.setAttribute('type', 'text');

    var wrapInput = document.createElement('div');
    wrapInput.classList.add('form-group');
    wrapInput.appendChild(input);
    this.bodyWindow.appendChild(wrapInput);

    input.classList.add('form-control');
    var buttonOk = document.createElement('button');
    input.setAttribute('type', 'text');
    this.bodyWindow.appendChild(buttonOk);
    buttonOk.classList.add('btn');
    buttonOk.classList.add('btn-default');
    var buttonCancel = buttonOk.cloneNode(false);
    buttonCancel.classList.add('space');
    buttonOk.textContent = 'Добре';
    buttonCancel.textContent = 'Відміна';

    this.bodyWindow.appendChild(buttonCancel);
    this.bodyWindow.appendChild(buttonOk);

    this.wrapInput = wrapInput;
    this.input = input;
    this.buttonOk = buttonOk;
    this.buttonCancel = buttonCancel;

    this.response.value = null;
    this.validetion = [];
    this.fnValid = {};
};

PromptModalWindow.prototype = Object.create(AlertModalWindow.prototype);
PromptModalWindow.prototype.constructor = PromptModalWindow;

PromptModalWindow.prototype._valid = function () {
    var valid = this.validetion;
    var res = this.response;
    for (var i = 0, bool = true; i < valid.length && bool; i++) {
        if (!res[valid[i].val]) {
            bool = false;
        }
    }

    if (bool) {
        this._cancel();
    } else {
        this.response.yes = false;
        this.fnValid[valid[i - 1].val]();
    }
};

PromptModalWindow.prototype._eventChange = function (el, fn) {
    var eventDestMsg = function (event) {
        fn();
        el.removeEventListener('change', eventDestMsg, false);
    };

    el.addEventListener('change', eventDestMsg, false);
};

PromptModalWindow.prototype.show = function (init, cb) {
    this.input.setAttribute('placeholder', init.placeholder);
    if (init.value) {
        this.input.setAttribute('value', init.value);
    }
    this.buttonCancel.onclick = this._cancel.bind(this);
    var that = this;

    var fnValue = that.fnValid['value'];
    this.fnValid['value'] = function () {
        if (fnValue) {
            fnValue();
        }
        that.bodyText.classList.add('error');
        that._eventChange(that.input, (function (el) {
            el.className = el.className.replace('error', '');
        }).bind(null, that.bodyText))
    };

    if (init.valueNeed) {
        this.validetion.push({val: 'value'})
    }

    this.buttonOk.onclick = function () {
        that.response.yes = true;
        that.response.value = that.input.value;
        that._valid();
    };

    AlertModalWindow.prototype.show.apply(this, arguments);
};

PromptModalWindow.prototype._addButton = function (classes) {
    var span = document.createElement('span');
    span.classList.add('input-group-btn');

    var btn = document.createElement('button');
    btn.classList.add('btn');
    btn.classList.add('btn-default');
    btn.setAttribute('type', 'button');

    var textBtn = document.createElement('span');
    classes.split(' ').map(function (cl) {
        textBtn.classList.add(cl)
    });

    btn.appendChild(textBtn);
    span.appendChild(btn);

    this.wrapInput.appendChild(span);

    return btn;
};

var PromptImgModalWindow = function () {
    PromptModalWindow.apply(this);

    this.wrapInput.classList.remove('form-group');
    this.wrapInput.classList.add('input-group');

    var imgBtn = this._addButton('glyphicon glyphicon-picture');

    var imgInput = document.createElement('input');
    imgInput.classList.add('btn');
    imgInput.classList.add('btn-default');
    imgInput.classList.add('hidden');
    imgInput.setAttribute('type', 'file');
    imgInput.setAttribute('accept', 'image/gif, image/jpg, image/jpeg, image/png');

    var buttonPreview = this.buttonOk.cloneNode(false);
    buttonPreview.classList.add('left');
    buttonPreview.classList.add('hidden');
    buttonPreview.textContent = 'Переглянути';

    var imgPreview = document.createElement('img');
    imgPreview.setAttribute('src', '');
    imgPreview.setAttribute('width', '');
    imgPreview.setAttribute('height', '');

    var imgCenter = document.createElement('dir');
    imgCenter.classList.add('img-center');
    imgCenter.hidden = true;
    imgCenter.appendChild(imgPreview);

    var dirImg = document.createElement('dir');
    dirImg.appendChild(imgCenter);

    this.bodyWindow.appendChild(buttonPreview);
    this.bodyWindow.appendChild(dirImg);
    this.wrapInput.appendChild(imgInput);

    this.wrapImg = imgCenter;
    this.imgPreview = imgPreview;
    this.buttonPreview = buttonPreview;
    this.imgBtn = imgBtn;
    this.imgInput = imgInput;

    this.previewShow = false;
    this.response.file = null;
};

PromptImgModalWindow.prototype = Object.create(PromptModalWindow.prototype);
PromptImgModalWindow.prototype.constructor = PromptImgModalWindow;

PromptImgModalWindow.prototype.clearHidden = function (el) {
    el.className = el.className.replace('hidden', '');
};

PromptImgModalWindow.prototype.show = function (init, cb) {
    var that = this;
    var listAddImg = [];

    var fnFile = that.fnValid['file'];
    this.fnValid['file'] = function () {
        if (fnFile) {
            fnFile();
        }
        that.imgBtn.click();
    };

    if (init.fileNeed) {
        this.validetion.push({val: 'file'})
    }

    if (init.img && init.img.length) {
        for (var i = 1, l = init.img.length; i < l; i++) {
            var img = this.imgPreview.cloneNode(false);
            img.width = init.img[i].width || 150;
            if (init.img[i].height) {
                img.height = init.img[i].height;
            }
            img.src = init.img[i].src;
            this.wrapImg.appendChild(img);

            listAddImg.push(img);
        }
        this.imgPreview.src = init.img[0].src;
        if (init.img[0].height) {
            this.imgPreview.height = init.img[0].height;
        }
        this.imgPreview.width = init.img[0].width;

        this.clearHidden(this.buttonPreview);
    }

    this.buttonPreview.onclick = function () {
        that.wrapImg.hidden = that.previewShow;
        that.previewShow = !that.previewShow;
    };

    this.imgInput.onchange = function () {
        that.response.file = that.imgInput.files[0];

        var reader = new FileReader();

        reader.onloadend = function () {
            that.imgPreview.src = reader.result;
        };
        reader.readAsDataURL(that.response.file);

        that.imgPreview.width = 200;
        that.imgPreview.removeAttribute('height');
        for (; listAddImg.length;) {
            that.wrapImg.removeChild(listAddImg.pop());
        }
        that.buttonPreview.className = that
            .buttonPreview
            .className
            .replace('hidden', '');
        this.value = '';
    };

    this.imgBtn.onclick = function () {
        that.imgInput.click();
    };

    PromptModalWindow.prototype.show.apply(this, arguments);
};


var PromptImgDesModalWindow = function () {
    PromptImgModalWindow.apply(this);

    var desBtn = this._addButton('glyphicon glyphicon-pencil');


    var wrapAbout = document.createElement('dir');
    var textArea = document.createElement('textarea');
    textArea.classList.add('form-control');

    wrapAbout.appendChild(textArea);
    wrapAbout.classList.add('hidden');

    this.bodyWindow.insertBefore(wrapAbout, this.wrapInput);

    var buttonBack = this.buttonOk.cloneNode(false);
    buttonBack.classList.add('left');
    buttonBack.classList.add('hidden');
    buttonBack.textContent = 'Певернутися';

    var bodyTextDes = document.createElement('p');
    bodyTextDes.classList.add('hidden');
    this.bodyWindow.insertBefore(bodyTextDes, this.bodyText);

    this.bodyWindow.insertBefore(buttonBack, this.buttonPreview);

    this.bodyTextDes = bodyTextDes;
    this.textArea = textArea;
    this.buttonBack = buttonBack;
    this.wrapAbout = wrapAbout;
    this.desBtn = desBtn;
    this.buttonBack = buttonBack;

    this.stateBtnPr = false;

    this.response.text = null;
};

PromptImgDesModalWindow.prototype = Object.create(PromptImgModalWindow.prototype);
PromptImgDesModalWindow.prototype.constructor = PromptImgDesModalWindow;

PromptImgDesModalWindow.prototype._setOnEvent = function (txt, btn, el, title) {
    var that = this;

    btn.onclick = function () {
        that.clearHidden(txt);
        txt.textContent = title;

        that.bodyText.classList.add('hidden');
        that.clearHidden(that.buttonBack);
        that.clearHidden(el);

        that.wrapInput.classList.add('hidden');

        if (!that.wrapImg.hidden) {
            that.buttonPreview.click();
        }

        if (!(that.buttonPreview.className.indexOf('hidden') + 1)) {
            that.stateBtnPr = true;
            that.buttonPreview.classList.add('hidden');
        }

    };
};

PromptImgDesModalWindow.prototype._backMain = function (wrap, txt) {
    if (txt.classList) {
        txt.classList.add('hidden');
    }
    this.buttonBack.classList.add('hidden');
    wrap.classList.add('hidden');
    this.clearHidden(this.bodyText);
    this.clearHidden(this.wrapInput);

    if (this.stateBtnPr) {
        this.clearHidden(this.buttonPreview);
    }
};

PromptImgDesModalWindow.prototype.show = function (init, cb) {
    var that = this;

    if (init.valueDes) {
        this.textArea.value = init.valueDes;
    }

    this.fnValid['file'] = function () {
        that.buttonBack.click();
    };
    this.fnValid['value'] = function () {
        that.buttonBack.click();
    };
    var fnText = this.fnValid['text'];
    this.fnValid['text'] = function () {
        if (fnText) {
            fnText();
        }
        that.desBtn.click();
        that.bodyTextDes.classList.add('error');
        that._eventChange(that.textArea, (function (el) {
            el.className = el.className.replace('error', '');
        }).bind(null, that.bodyTextDes))
    };

    if (init.textNeed) {
        this.response.text = init.valueDes;
        this.validetion.push({val: 'text'})
    }

    this._setOnEvent(that.bodyTextDes, this.desBtn, that.wrapAbout, init.titleDes);

    this.textArea.onchange = function () {
        that.response.text = that.textArea.value;
    };

    this.buttonBack
        .addEventListener('click', that._backMain.bind(that, that.wrapAbout, that.bodyTextDes), false);

    PromptImgModalWindow.prototype.show.apply(this, arguments);
};


var PromptImgDesChModalWindow = function () {
    PromptImgDesModalWindow.apply(this);

    var chBtn = this._addButton('glyphicon glyphicon-th-list');

    var wrapCh = document.createElement('dir');
    var inputCh = document.createElement('input');
    inputCh.classList.add('form-control');

    wrapCh.appendChild(inputCh);
    wrapCh.classList.add('hidden');

    var bodyTextCh = document.createElement('p');
    bodyTextCh.classList.add('hidden');
    this.bodyWindow.insertBefore(bodyTextCh, this.bodyText);

    this.bodyWindow.insertBefore(wrapCh, this.wrapInput);

    this.bodyTextCh = bodyTextCh;
    this.inputCh = inputCh;
    this.wrapCh = wrapCh;
    this.chBtn = chBtn;

    this.response.charact = null;

};

PromptImgDesChModalWindow.prototype = Object.create(PromptImgDesModalWindow.prototype);
PromptImgDesChModalWindow.prototype.constructor = PromptImgDesChModalWindow;

PromptImgDesChModalWindow.prototype.show = function (init, cb) {
    var that = this;

    if (init.valueCh) {
        this.inputCh.setAttribute('value', init.valueCh);
    }

    this.inputCh.placeholder = init.placeholderCh;
    this.fnValid['file'] = function () {
        that.buttonBack.click();
    };
    this.fnValid['value'] = function () {
        that.buttonBack.click();
    };
    this.fnValid['text'] = function () {
        that.buttonBack.click();
    };
    var fnCharact = this.fnValid['charact'];
    this.fnValid['charact'] = function () {
        if (fnCharact) {
            fnCharact();
        }
        that.buttonBack.click();
        that.chBtn.click();
        that.bodyTextCh.classList.add('error');
        that._eventChange(that.inputCh, (function (el) {
            el.className = el.className.replace('error', '');
        }).bind(null, that.bodyTextCh))
    };

    if (init.charactNeed) {
        this.validetion.push({val: 'charact'})
    }
    this._setOnEvent(that.bodyTextCh, this.chBtn, this.wrapCh, init.titleCh);
    this.buttonBack.onclick = this._backMain.bind(this, this.bodyTextCh);
    this.inputCh.onchange = function () {
        that.response.charact = that.inputCh.value;
    };

    this.buttonBack
        .addEventListener('click', that._backMain.bind(that, that.wrapCh, that.bodyTextCh), false);

    PromptImgDesModalWindow.prototype.show.apply(this, arguments);
};