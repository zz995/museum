(function () {
    function showListDocView(input) {
        var files = input.files;
        var localDoc = document.getElementById('localDoc');
        var wrap = localDoc.cloneNode(false);
        for (var i = 0, l = files.length; i < l; i++) {
            var file = files[i];
            var div = document.createElement('div');
            div.className = 'wrap';

            var p = document.createElement('p');
            p.textContent = files[i].name;
            div.appendChild(p);

            var textarea = document.createElement('textarea');
            textarea.className = 'form-control';
            textarea.placeholder = 'Введіть опис даного документу';
            textarea.row = 3;
            textarea.name = 'descripDoc[' + files[i].name + ']';
            div.appendChild(textarea);

            wrap.appendChild(div);
        }
        localDoc.parentNode.replaceChild(wrap, localDoc);
    }

    document.getElementById('localDocId').onchange = function () {
        showListDocView(this);
    };
    showListDocView(document.getElementById('localDocId'));
})();