(function () {
    var FOLDER = [];
    var SELECTED = {};
    var SORT;
    var alertDanger = document.getElementById('alertDanger');
    var bodyTable = document.getElementById('bodyTable');
    var TR = document.createElement('tr');
    TR.innerHTML = '<tr><th class="col-xs-1"></th><td class="col-xs-1"><span class=""></span></td><td class="col-xs-5 title"></td><td class="col-xs-2"><span></span></td><td class="col-xs-3"></td></tr>';
    var DATA = {};

    function showAlertView(alert, msg) {
        alert.hidden = false;
        alert.textContent = msg;
        setTimeout(function () {
            alert.hidden = true;
            alert.textContent = '';
        }, 3000);
    }

    function getPathName() {
        var folder = FOLDER.join('/');
        if (folder) {
            folder += '/';
        }
        return '/' + folder + SELECTED.name
    }

    function rebuildTbodyView(data) {
        clearSelectView();

        var tbody = document.createElement('tbody');
        for (var i = 1, l = data.length; i <= l; i++) {
            var tr = TR.cloneNode(true);
            var td = tr.firstChild;
            td.textContent = i;
            td = td.nextSibling;
            td.innerHTML = data[i - 1].isFile ?
                '<span class="glyphicon glyphicon-picture img"></span>' :
                '<span class="glyphicon glyphicon-folder-close dir"></span>';
            td = td.nextSibling;
            td.textContent = data[i - 1].name;
            td = td.nextSibling;
            td.textContent = data[i - 1].isFile ? data[i - 1].size : '-';
            td = td.nextSibling;
            td.textContent = data[i - 1].mtime;
            tbody.appendChild(tr);
        }
        document.getElementById('tableElem').replaceChild(tbody, bodyTable);
        bodyTable = tbody;
        bodyTable.onclick = select;
        bodyTable.ondblclick = dbClick;
    }

    function uploadImg(file) {
        var formData = new FormData();
        var xhr = new XMLHttpRequest();

        formData.append('globalImg', file.files[0], file.files[0].name);
        formData.append('path', '/' + FOLDER.join('/'));

        xhr.open('POST', '/admin/exhibit/image', true);

        xhr.onreadystatechange = function () {
            if (xhr.readyState != 4) return;
            var ans = JSON.parse(xhr.responseText);
            if (xhr.status === 200) {
                refreshList();
            } else {
                showAlertView(alertDanger, ans.message);
            }
        };

        xhr.send(formData);
    }

    function refreshList(event, cb) {
        if (event) {
            moveElementView(false);
        }

        var xhr = new XMLHttpRequest();
        var params = 'path=' + encodeURIComponent('/' + FOLDER.join('/'));

        xhr.open("GET", '/admin/exhibit/image?' + params, true);

        xhr.onreadystatechange = function () {
            if (xhr.readyState != 4) return;
            var ans = JSON.parse(xhr.responseText);
            if (xhr.status === 200) {
                DATA.data = ans.list;
                if (cb === undefined) {
                    rebuildTbodyView(ans.list);
                } else {
                    cb(ans.list);
                }
            } else {
                showAlertView(alertDanger, ans.message);
            }
        };

        xhr.send();

        return false;
    }

    function selectView() {
        if (SELECTED.old) {
            SELECTED.old.className = SELECTED.old.className.replace(/\s?ground/, '');
        }
        SELECTED.elem.classList.add('ground');
        var imgElem = document.getElementById('image');
        var url = document.getElementById('url');
        var src = '/' + GLOBAL_PATH_IMAGES + getPathName();
        if (SELECTED.isFile) {
            imgElem.hidden = false;
            imgElem.src = src;
            url.hidden = false;
            url.textContent = src;
        } else {
            imgElem.hidden = true;
            url.hidden = true;
        }
    }

    function clearSelectView() {
        if (SELECTED.elem) {
            SELECTED.elem.className = SELECTED.elem.className.replace(/\s?ground/, '');
        }
        if (SELECTED.isFile) {
            document.getElementById('image').hidden = true;
            document.getElementById('url').hidden = true;
        }

        SELECTED.elem = null;
        SELECTED.old = null;
        SELECTED.name = null;
        SELECTED.isFile = null;
    }

    function select(event) {
        var target = event.target;
        for (; target.tagName !== 'TR' && target.tagName !== bodyTable.tagName; target = target.parentNode) {
        }

        if (target.tagName === 'TR') {
            SELECTED.old = SELECTED.elem;
            SELECTED.elem = target;

            if (SELECTED.old !== SELECTED.elem) {
                SELECTED.name = target.getElementsByClassName('title')[0].innerHTML;
                SELECTED.isFile = !!target.getElementsByClassName('img').length;
                selectView();
            }
        }
    }

    function changeDirView() {
        if (SELECTED.name && !SELECTED.isFile && !SELECTED.returnBack) {
            FOLDER.push(SELECTED.name);
        }
        SELECTED.returnBack = false;

        refreshList();
        var olOld = document.getElementById('path');
        var ol = olOld.cloneNode(false);
        var li;
        var l = FOLDER.length - 1;

        if (l + 1) {
            li = document.createElement('li');
            li.innerHTML = '<a href="0">' + 'Корінь' + '</a>';
            ol.appendChild(li);

            for (var i = 1; i <= l; i++) {
                li = document.createElement('li');
                li.innerHTML = '<a href="' + i + '">' + FOLDER[i - 1] + '</a>';
                ol.appendChild(li);
            }

            li = document.createElement('li');
            li.textContent = FOLDER[l];
            ol.appendChild(li);

        } else {
            li = document.createElement('li');
            li.classList.add('active');
            li.textContent = 'Корінь';
            ol.appendChild(li);
        }

        olOld.parentNode.replaceChild(ol, olOld);

        ol.onclick = returnBack;
    }

    function dbClick(event) {
        DATA.sort = -1;
        if (SELECTED.isFile) {
            var img = '<img src="' + '/' + GLOBAL_PATH_IMAGES + getPathName() + '">';
            tinymce.activeEditor.execCommand('mceInsertContent', false, img);
        } else {
            changeDirView();
        }
    }

    function returnBack(event) {
        var target = event.target;

        if (target.tagName === 'A') {
            var len = target.href.match(/\d(?=$)/)[0] - 0;
            FOLDER = FOLDER.splice(0, len);
            DATA.sort = -1;
            SELECTED.returnBack = true;
            changeDirView();
        }

        return false;
    }

    function deleteElement(event) {
        if (SELECTED.name) {
            var xhr = new XMLHttpRequest();

            xhr.open("DELETE", '/admin/exhibit/image', true);

            var body = 'path=' + encodeURIComponent(getPathName());

            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.send(body);

            xhr.onreadystatechange = function () {
                if (xhr.readyState != 4) return;
                var ans = JSON.parse(xhr.responseText);
                if (xhr.status === 200) {
                    if (SELECTED.moveName == getPathName()) {

                        moveElementView(false);
                    }
                    refreshList();
                } else {
                    showAlertView(alertDanger, ans.message);
                }
            };
        }

        return false;
    }

    function createDir(event) {
        var modal = new PromptModalWindow();
        var init = {
            head: 'Добавлення нової папки',
            body: 'Введіть назву папки',
            placeholder: 'Назва папки',
            valueNeed: true
        };

        modal.show(init, function (ob) {
            var name = ob.value;
            if (name) {
                var xhr = new XMLHttpRequest();

                xhr.open("POST", '/admin/exhibit/image/dir', true);

                var body = 'path=' + encodeURIComponent('/' + FOLDER.join('/')) +
                    '&name=' + encodeURIComponent(name);

                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                xhr.send(body);

                xhr.onreadystatechange = function () {
                    if (xhr.readyState != 4) return;
                    var ans = JSON.parse(xhr.responseText);
                    if (xhr.status === 200) {
                        refreshList();
                    } else {
                        showAlertView(alertDanger, ans.message);
                    }
                };
            }
        });

        return false;
    }

    function renameElement(event) {
        if (SELECTED.name) {
            var modal = new PromptModalWindow();
            var init = {
                head: 'Переіменування',
                body: 'Введіть нову назву',
                placeholder: 'Назва',
                valueNeed: true
            };

            modal.show(init, function (ob) {
                var name = ob.value;
                if (name) {
                    var xhr = new XMLHttpRequest();

                    xhr.open("PUT", '/admin/exhibit/image', true);

                    var body = 'path=' + encodeURIComponent(getPathName()) +
                        '&name=' + encodeURIComponent(name);

                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send(body);

                    xhr.onreadystatechange = function () {
                        if (xhr.readyState != 4) return;
                        var ans = JSON.parse(xhr.responseText);
                        if (xhr.status === 200) {
                            refreshList();
                        } else {
                            showAlertView(alertDanger, ans.message);
                        }
                    };
                }
            });
        }

        return false;
    }

    function moveElementView(bool) {
        var move = document.getElementById('moveElement').firstChild;

        if (bool) {
            move.className = move.className.replace('glyphicon-random', 'glyphicon-save');
            SELECTED.moveName = getPathName();
        } else {
            move.className = move.className.replace('glyphicon-save', 'glyphicon-random');
            SELECTED.moveName = undefined;
            SELECTED.moveElem = undefined;
        }

    }

    function moveElement(event) {
        if (SELECTED.name && !SELECTED.moveName) {
            moveElementView(true);
        } else {
            if (SELECTED.moveName) {
                var xhr = new XMLHttpRequest();

                xhr.open("PUT", '/admin/exhibit/image/move', true);

                var body = 'path=' + encodeURIComponent(SELECTED.moveName) +
                    '&newPath=' + encodeURIComponent('/' + FOLDER.join('/'));

                moveElementView(false);

                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                xhr.send(body);

                xhr.onreadystatechange = function () {
                    if (xhr.readyState != 4) return;
                    var ans = JSON.parse(xhr.responseText);
                    if (xhr.status === 200) {
                        refreshList();
                    } else {
                        showAlertView(alertDanger, ans.message);
                    }
                };
            }
        }

        return false;
    }

    function sort(field, compare) {
        DATA.sort = (DATA.field == field) && DATA.sort ? ~DATA.sort + 1 : 1;
        data = DATA.data;
        if (data) {
            data.sort(compare);
            rebuildTbodyView(data);
        } else {
            refreshList(false, function (val) {
                val.sort(compare);
                rebuildTbodyView(val);
            })
        }
    }

    function sortType() {
        sort('type', function (a, b) {
            if (a.isFile > b.isFile) return -DATA.sort;
            if (a.isFile < b.isFile) return DATA.sort;
        });

        DATA.field = 'type';
    }

    function sortName() {
        sort('name', function (a, b) {
            return new Intl.Collator('uk-UA').compare(a.name, b.name) * DATA.sort;
        });

        DATA.field = 'name';
    }

    function sortSize() {
        sort('size', function (a, b) {
            if (a.isFile > b.isFile) return -DATA.sort;
            if (a.isFile < b.isFile) return DATA.sort;
        });

        DATA.field = 'size';
    }

    function sortDate() {
        sort('date', function (a, b) {
            return a.mtime.localeCompare(b.mtime) * DATA.sort;
        });

        DATA.field = 'date';
    }

    document.getElementById('uploadImg').onclick = function (event) {
        document.getElementById('globalImgId').click();
        return false;
    };
    document.getElementById('globalImgId').onchange = function () {
        uploadImg(this);
        this.value = '';
    };
    document.getElementById('refreshList').onclick = refreshList;
    document.getElementById('deleteElement').onclick = deleteElement;
    document.getElementById('createDir').onclick = createDir;
    document.getElementById('renameElement').onclick = renameElement;
    document.getElementById('moveElement').onclick = moveElement;
    document.getElementById('typeGlImg').onclick = sortType;
    document.getElementById('nameGlImg').onclick = sortName;
    document.getElementById('sizeGlImg').onclick = sortSize;
    document.getElementById('dateGlImg').onclick = sortDate;
    bodyTable.onclick = select;
    bodyTable.ondblclick = dbClick;

})();
