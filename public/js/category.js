(function () {

    function showHideView(li) {
        [].forEach.call(li.getElementsByTagName('ul'), function (item) {
            item.hidden = !item.hidden;
        });
    }

    function addCatView(id, name, li) {
        var elemLi = document.createElement('li');
        var infoNeed = li.tagName != 'LI';
        for (var elem = li.firstChild, firstElem = true; elem && elem.tagName !== 'UL'; elem = elem.nextSibling) {
            if (firstElem) {
                firstElem = false;
                elemLi.innerHTML +=
                    elem.outerHTML
                        .replace(/id\s*=\s*("|')_[a-f0-9]+\1/, 'id="_' + id + '"')
                        .replace(/(>).+(?=<)/, '> ' + name);
            } else {
                elemLi.innerHTML += elem.outerHTML;
            }
        }

        elemLi.getElementsByClassName('showCat')[0].parentNode.hidden = true;
        elemLi.getElementsByClassName('addCat')[0].parentNode.hidden = false;
        elemLi.getElementsByClassName('ranCat')[0].parentNode.hidden = false;
        elemLi.getElementsByClassName('delCat')[0].parentNode.hidden = false;
        var info = elemLi.getElementsByClassName('info')[0].parentNode;
        info.hidden = !infoNeed;
        info.href = info.href.replace(/\/undefined(?=$|\?)/, '/' + id);

        li.getElementsByClassName('showCat')[0].parentNode.hidden = false;
        li.getElementsByClassName('delCat')[0].parentNode.hidden = true;

        var ul = li.getElementsByTagName('ul')[0];
        if (ul) {
            ul.appendChild(elemLi);
        } else {
            ul = document.createElement('ul');
            ul.appendChild(elemLi);
            li.appendChild(ul);
        }
    }

    function addCat(id, li) {
        var modal = new PromptImgDesChModalWindow();
        var init = {
            head: 'Добавлення нової категорії',
            body: 'Введіть назву категорії',
            placeholder: 'Категорія',
            placeholderCh: 'Характеристики повинні бути розділенні комами',
            titleDes: 'Опис категорії',
            titleCh: 'Характеристики категорії',
            valueNeed: true,
            fileNeed: true,
            textNeed: true,
            value: 'Невідомо'
        };

        modal.show(init, function (ob) {
            if (ob && ob.yes) {

                var formData = new FormData();
                var xhr = new XMLHttpRequest();

                id = id.slice(1);

                formData.append('categoryImg', ob.file);
                formData.append('title', ob.value);
                formData.append('body', ob.text);
                if (ob.charact) {
                    formData.append('charact', ob.charact);
                }
                formData.append('id', id);

                xhr.open('POST', '/admin/category', true);

                xhr.onreadystatechange = function () {
                    if (xhr.readyState != 4) return;
                    var ans = JSON.parse(xhr.responseText);
                    if (xhr.status === 200) {
                        (new AlertModalWindow()).show({
                            head: 'Добавлення нової категорії',
                            body: ans.message
                        });
                        addCatView(ans.id, ans.name, li);
                    } else {
                        (new ErrorModalWindow()).show({
                            head: 'Добавлення нової категорії',
                            body: ans.message
                        });
                    }
                };

                xhr.send(formData);
            }
        });
    }

    function updateElemView(name, elem) {
        elem.textContent = name;
    }

    function updateCat(id, li) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/api/category/' + id.slice(1), true);
        xhr.send();
        xhr.onreadystatechange = function () {
            if (xhr.readyState != 4) return;
            var info = JSON.parse(xhr.responseText);
            if (xhr.status !== 200) {
                (new ErrorModalWindow()).show({
                    head: 'Редагування категорії',
                    body: info.message
                });
            } else {
                showWindowForChange(info);
            }
        };

        function showWindowForChange(info) {
            var modal = new PromptImgDesChModalWindow();
            var init = {
                head: 'Редагування категорії',
                body: 'Назва категорії',
                value: info.name,
                valueCh: info.charact.join(', '),
                valueDes: info.body,
                placeholder: 'Категорія',
                placeholderCh: 'Характеристики повинні бути розділенні комами',
                titleDes: 'Опис категорії',
                titleCh: 'Характеристики категорії',
                img: [
                    {
                        src: info.img.path + info.img.extname,
                        width: 200
                    },
                    {
                        src: info.img.path + '_sm.jpg',
                        width: 200
                    },
                    {
                        src: info.img.path + '_md.jpg',
                        width: 400
                    }
                ],
                valueNeed: true,
                textNeed: true
            };
            modal.show(init, function (ob) {
                if (ob && ob.yes) {
                    var formData = new FormData();
                    var xhr = new XMLHttpRequest();

                    formData.append('id', id);
                    if (ob.value && ob.value !== info.name) formData.append('title', ob.value);
                    if (ob.file) formData.append('categoryImg', ob.file);
                    if (ob.text) formData.append('body', ob.text);
                    if (ob.charact) formData.append('charact', ob.charact);

                    xhr.open('PUT', '/admin/category/' + id.slice(1), true);

                    xhr.onreadystatechange = function () {
                        if (xhr.readyState != 4) return;
                        var ans = JSON.parse(xhr.responseText);
                        if (xhr.status === 200) {
                            (new AlertModalWindow()).show({
                                head: 'Редагування категорії',
                                body: ans.message
                            });
                            updateElemView(ans.name, li.firstChild);
                        } else {
                            (new ErrorModalWindow()).show({
                                head: 'Редагування категорії',
                                body: ans.message
                            });
                        }
                    };

                    xhr.send(formData);
                }
            })
        }
    }

    function deleteCatView(li) {
        var ul = li.parentNode;
        var ulli = ul.parentNode;
        var childLi = li.firstChild;
        for (; childLi; childLi = childLi.nextSibling) {
            if (childLi.tagName === 'UL') {
                var childUl = childLi.firstChild;
                for (; childUl; childUl = nextChildUl) {
                    var nextChildUl = childUl.nextSibling;
                    ul.appendChild(childUl);
                }
                break;
            }
        }
        if (!(ul.childElementCount - 1)) {
            ulli.removeChild(ul);
            ulli.getElementsByClassName('showCat')[0].parentNode.hidden = true;
            if (ulli.tagName == 'LI') {
                ulli.getElementsByClassName('delCat')[0].parentNode.hidden = false;
            }
        } else {
            ul.removeChild(li);
        }
    }

    function deleteCat(id, li) {
        var modal = new ConfirmModalWindow();
        var init = {
            head: 'Підтвердження видалення категорії',
            body: 'Видалити?'
        };
        modal.show(init, function (ob) {
            if (ob && ob.yes) {
                var xhr = new XMLHttpRequest();
                xhr.open("DELETE", '/admin/category/' + id.slice(1), true);
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                xhr.send();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState != 4) return;
                    try {
                        var ans = JSON.parse(xhr.responseText);
                    } catch (e) {
                        xhr.status = 0;
                        ans = {message: 'Відбулася помилка'};
                    }

                    if (xhr.status === 200) {
                        (new AlertModalWindow()).show({
                            head: 'Видалення категорії',
                            body: ans.message
                        });
                        deleteCatView(li);
                    } else {
                        (new ErrorModalWindow()).show({
                            head: 'Видалення категорії',
                            body: ans.message
                        });
                    }
                };
            }
        });
    }

    function addCatList(id, name) {
        var elem = document.getElementById('listCateg');
        var listName = document.getElementById('nameListCateg');
        if (elem && listName) {
            id = id.slice(1);
            if (!(elem.value.indexOf(id) + 1)) {
                if (!elem.value) {
                    elem.value = id;
                    listName.value = name;
                } else {
                    elem.value += ', ' + id;
                    listName.value += ', ' + name;
                }
            } else {
                elem.value = elem.value.replace(id, '')
                    .replace(/^,\s|,\s(?=,\s|$)/, '');
                listName.value = listName.value.replace(name, '')
                    .replace(/^,\s|,\s(?=,\s|$)/, '');
            }
        }
    }

    function handleClick(action, id, li, target) {
        switch (action) {
            case 1:
                showHideView(li);
                break;
            case 2:
                addCat(id, li);
                break;
            case 3:
                updateCat(id, li);
                break;
            case 4:
                deleteCat(id, li);
                break;
            case 0:
                addCatList(id, li.firstChild.textContent);
                break;
            default:
                break;
        }
    }

    document.getElementById('category').onclick = function (event) {
        var target = event.target;
        if (target.tagName === 'SPAN') {
            var findClassList = [].indexOf.bind(target.classList);
            if (findClassList('info') + 1) return true;
            var action =
                (findClassList('showCat') + 1) ? 1 :
                    (findClassList('addCat') + 1) ? 2 :
                        (findClassList('ranCat') + 1) ? 3 :
                            (findClassList('delCat') + 1) ? 4 : 0;
            var li = target.parentNode;
            if (action) {
                li = li.parentNode.parentNode;
            } else {
                if (window.CATEGORYNEED != 'full') return true;
            }
            handleClick(action, li.firstChild.id, li);
        }

        return false;
    };

    if (window.DragZone && window.DropTarget) {
        var categories = document.getElementById('category');
        new DragZone(categories);
        new DropTarget(categories);
    }
})();
