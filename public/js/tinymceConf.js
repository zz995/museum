tinymce.init({
    selector: '.description',
    language: 'uk_UA',
    relative_urls: false,
    height: 300,
    plugins: [
        "advlist autolink lists image print preview",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime table contextmenu paste imagetools"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    imagetools_cors_hosts: ['www.tinymce.com', 'codepen.io'],
    content_css: [
        '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
        '//www.tinymce.com/css/codepen.min.css'
    ]
});
