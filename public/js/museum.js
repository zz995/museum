(function () {

    document.getElementById('destroy').onclick = function () {
        var modal = new ConfirmModalWindow();
        var init = {
            head: 'Підтвердження видалення відомостей про музей',
            body: 'Видалити?'
        };
        modal.show(init, function (ob) {
            if (ob && ob.yes) document.getElementById('destroyForm').submit();
        });
        return false;
    };

})();
