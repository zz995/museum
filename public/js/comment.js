(function () {
    function addSortParam() {
        var href = document.getElementById('idButAppendSort').href;
        href = href.replace(/((&|\?)sort=)createdAt_-?1/, '$1' + this.value);
        document.getElementById('idButAppendSort').href = href;
    }

    function addCrtParam() {
        var href = document.getElementById('idButAppendSort').href;
        if (this.value === 'all') {
            href = href.replace(/(&|\?)visible=[^&|$]+/, '');
        } else if (this.value === 'visibleTrue') {
            if (href.indexOf('&visible=') + 1) {
                href = href.replace(/((&|\?)visible=)false/, '$1true');
            } else {
                href += '&visible=true';
            }
        } else if (this.value === 'visibleFalse') {
            if (href.indexOf('&visible=') + 1) {
                href = href.replace(/((&|\?)visible=)true/, '$1false');
            } else {
                href += '&visible=false';
            }
        }
        document.getElementById('idButAppendSort').href = href;
    }

    document.getElementById('sortComment').onchange = addSortParam;
    document.getElementById('crtComment').onchange = addCrtParam;
})();