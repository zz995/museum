'use strict';

exports.login = function (req, res, next) {
    if (req.isAuthenticated()) {
        req.isRoot = req.user.adminname == 'root';
        return next();
    }
    if (req.method == 'GET') req.session.returnTo = req.originalUrl;
    res.redirect('/admin/login');
};

exports.checkAccess = function (req, res, next) {
    if (req.isAuthenticated() && req.admin._id.toString() == req.user._id.toString()) {
        req.isRoot = req.user.adminname == 'root';
        return next();
    }
    res.redirect('/admin');
};

exports.rootAccess = function (req, res, next) {
    if (req.isAuthenticated() && req.admin._id.toString() == req.user._id.toString()) {
        req.isRoot = req.user.adminname == 'root';
        if (req.isRoot) return next();
    }
    res.redirect('/admin');
};