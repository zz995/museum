'use strict';

const HttpError = require('../libs/error').HttpError;

module.exports = function (req, res, next) {

    res.sendDataClient = function (data, template) {
        let bool = req.answerNeedJSON;
        if (data instanceof HttpError || data instanceof Error) {
            let obSend = {
                message: data.message
            };
            res.status(data.status);
            if (bool) {
                res.json(obSend);
            } else {
                obSend.admin = req.user;
                obSend.error = process.env.NODE_ENV === 'development' ? data : {};
                res.render(template || (req.isAuthenticated() ? 'errorAdm' : 'error'), obSend);
            }
        } else {
            bool = bool || !template;
            if (bool) {
                res.json(data);
            } else {
                res.render(template, data);
            }
        }
    };

    next();
};