'use strict';

module.exports = function (req, res, next) {
    req.answerNeedJSON = true;

    next();
};
