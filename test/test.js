'use strict';

process.env.NODE_ENV = 'test';

const request = require('supertest');
const should = require('should');
const url = `http://localhost:${require('../config').get('port')}`;
const agent = request.agent(url);
const mongoose = require('mongoose');
const config = require('../config');
const path = require('path');

const _user = {
    adminname: 'root',
    password: 'root'
};

describe('Exhibit - category', function () {
    this.timeout(10000);
    it('POST /admin/category - when not logged in - should was Error', function (done) {
        agent
            .post('/admin/category')
            .attach('categoryImg', path.join(__dirname, 'files/test.JPG'))
            .field('title', 'test category')
            .field('body', 'test category')
            .field('id', '0')
            .expect('Content-Type', /plain/)
            .expect(302)
            .expect('Location', '/admin/login')
            .expect(/Redirecting/)
            .end(function (err, res) {
                if (err) return done();
                done(err);
            });
    });
    it('Should create a session', function (done) {
        agent
            .post('/admin/session')
            .send(_user)
            .expect(function (res) {
                if (!(res.res.statusCode == 201 || res.res.statusCode == 302)) {
                    throw new Error('Status code is')
                }
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            })
    });
    it('POST /admin/category - invalid form', function (done) {
        agent
            .post('/admin/category')
            .field('title', 'test category')
            .field('body', 'test category')
            .field('id', '0')
            .expect('Content-Type', /json/)
            .expect(400, done);
    });
    let idc = [];
    it('POST /admin/category - valid form', function (done) {
        agent
            .post('/admin/category')
            .attach('categoryImg', path.join(__dirname, 'files/test.JPG'))
            .field('title', 'test category')
            .field('body', 'test category')
            .field('id', '0')
            .expect(200)
            .expect(function (res) {
                res.should.be.an.instanceOf(Object);
                res.body.should.have.property('id');
                res.body.should.have.property('name', 'test category');
            })
            .end(function (err, res) {
                if (err) return done(err);
                idc.push(res.body.id);
                done();
            });
    });
    it('POST /admin/exhibit - when not logged in - should redirect to /admin/login', function (done) {
        request(url)
            .post('/admin/exhibit')
            .attach('mainPicture', path.join(__dirname, 'files/test.JPG'))
            .field('name', 'test exhibit')
            .field('arrayCats', idc[0])
            .expect('Content-Type', /plain/)
            .expect(302)
            .expect('Location', '/admin/login')
            .expect(/Redirecting/, done);
    });
    it('POST /admin/exhibit - invalid form', function (done) {
        agent
            .post('/admin/exhibit')
            .field('name', 'test exhibit')
            .field('arrayCats', idc[0])
            .expect(302)
            .expect('Location', '/admin/exhibit')
            .expect(/Redirecting/, done);
    });
    it('POST /admin/exhibit - valid form', function (done) {
        agent
            .post('/admin/exhibit')
            .attach('mainPicture', path.join(__dirname, 'files/test.JPG'))
            .field('name', 'test exhibit')
            .field('arrayCats', idc[0])
            .field('tags', 'tag 1, tag 2')
            .field('dateStarted', '1900')
            .field('dateFinish', '2050')
            .field('characteristics[0][name]', 'available')
            .field('characteristics[0][value]', 'yes')
            .field('bodyShort', 'bodyShort exhibit test')
            .field('body', 'body exhibit test')
            .expect(302)
            .expect('Location', '/admin/exhibit')
            .expect(/Redirecting/)
            .end(function (err, res) {
                if (err) return done(err);
                setTimeout(() => {
                    done();
                }, 2000);
            })
    });
    it('POST /admin/category - valid form - subcategory', function (done) {
        agent
            .post('/admin/category')
            .attach('categoryImg', path.join(__dirname, 'files/test.JPG'))
            .field('title', 'test category')
            .field('body', 'test category')
            .field('id', idc[0])
            .expect(200)
            .expect(function (res) {
                res.should.be.an.instanceOf(Object);
                res.body.should.have.property('id');
                res.body.should.have.property('name', 'test category');
                idc.push(res.body.id);
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            });
    });
    it(`PUT /admin/category/:id/move - move category`, function (done) {
        agent
            .put(`/admin/category/${idc[0]}/move`)
            .send({'id': '0'})
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function (res) {
                res.should.be.an.instanceOf(Object);
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            });
    });
    it(`GET /api/tags - API - get tags`, function (done) {
        request(url)
            .get('/api/tags')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function (res) {
                should.exist(res);
                res.body.should.be.an.instanceOf(Object);
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            });
    });
    it(`GET /api/tags/:id - API - search exhibit by tag`, function (done) {
        request(url)
            .get(`/api/tags/tag%201`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function (res) {
                should.exist(res);
                res.body.should.be.an.instanceOf(Object);
            })
            .end(function (err, res) {              
                if (err) return done(err);
                done();
            });
    });
    it(`GET /api/exhibit/category/:id - API - search exhibit by category`, function (done) {
        request(url)
            .get(`/api/exhibit/category/${idc[0]}`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function (res) {
                should.exist(res);
                res.body.should.be.an.instanceOf(Object);
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            });
    });
    it(`GET /api/exhibit - API - get exhibits list`, function (done) {
        request(url)
            .get('/api/exhibit')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
    let idExh = null;
    it(`GET /api/exhibit - API - search exhibit`, function (done) {
        request(url)
            .get('/api/exhibit')
            .query('chname=available&value=yes&search=test%20exhibit')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function (res) {
                should.exist(res);
                res.body.should.have.property('count').which.is.a.Number();
                res.body.should.have.property('exhibits').which.is.a.Array().and.not.be.empty();
                idExh = res.body.exhibits[0]._id;
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            });
    });
    it('PUT /admin/exhibit/:id - change exhibit', function (done) {
        agent
            .put(`/admin/exhibit/${idExh}`)
            .attach('mainPicture', path.join(__dirname, 'files/test.JPG'))
            .attach('altModel3d', path.join(__dirname, 'files/test.JPG'))
            .field('name', 'test exhibit change name')
            .field('countFrame', '21')
            .field('arrayCats', idc[0])
            .field('tags', 'tag 3, tag 4')
            .field('dateStarted', '2050')
            .field('dateFinish', '2100')
            .field('characteristics[0][name]', 'available')
            .field('characteristics[0][value]', 'no')
            .expect(302)
            .expect('Location', '/admin/exhibit')
            .expect(/Redirecting/)
            .end(function (err, res) {
                if (err) return done(err);
                setTimeout(() => {
                    done();
                }, 2000);
            });
    });
    it(`GET /api/exhibit/:id - API - get not exist exhibit`, function (done) {
        request(url)
            .get(`/api/exhibit/45135`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(400, done);
    });
    it(`GET /api/exhibit/:id - API - get exhibit`, function (done) {
        request(url)
            .get(`/api/exhibit/${idExh}`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function (res) {
                should.exist(res);
                res.body.should.have.property('exhibit').which.is.a.Object().and.not.be.empty();
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            });
    });
    it(`DELETE /admin/exhibit/:id - delete exhibit`, function (done) {
        agent
            .delete(`/admin/exhibit/${idExh}`)
            .expect(302)
            .expect('Location', '/admin/exhibit')
            .expect(/Redirecting/, done);
    });
    it('POST /admin/museum/:id - valid form', function (done) {
        agent
            .put(`/admin/museum/${idc[0]}`)
            .send({'name': 'Test name museum', 'body': 'Test body museum'})
            .expect(302)
            .expect('Location', `/admin/museum/${idc[0]}`)
            .expect(/Redirecting/, done);
    });
    it(`GET /admin/museum/:id - API - get info by museum`, function (done) {
        request(url)
            .get(`/api/museum/${idc[0]}`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function (res) {
                should.exist(res);
                res.body.should.have.property('name', 'Test name museum');
                res.body.should.have.property('body', 'Test body museum');
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            });
    });
    it('DELETE /admin/museum/:id - delete info', function (done) {
        agent
            .delete(`/admin/museum/${idc[0]}`)
            .expect(302)
            .expect('Location', `/admin/museum/${idc[0]}`)
            .expect(/Redirecting/, done);
    });
    it(`GET /api/category/:id - API - get category info`, function (done) {
        request(url)
            .get(`/api/category/${idc[0]}`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function (res) {
                should.exist(res);
                res.should.be.an.instanceOf(Object);
                res.body.should.have.property('body', 'test category');
                res.body.should.have.property('name', 'test category');
                should.exist(res.body.img);
                res.body.img.should.have.property('extname', '.JPG');
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            });
    });
    it(`PUT /admin/category/:id - change name`, function (done) {
        agent
            .put(`/admin/category/${idc[0]}`)
            .send({title: 'test category change name', id: idc[0]})
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function (res) {
                should.exist(res);
                res.should.be.an.instanceOf(Object);
                res.body.should.have.property('name', 'test category change name');
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            });
    });
    it(`GET /api/category - API - get categories list`, function (done) {
        request(url)
            .get('/api/category')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function (res) {
                should.exist(res);
                res.body.should.matchEach(
                        value => value.should.have.keys([ '_id', 'parent', 'name' ])
                );
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            });
    });
    it(`GET /api/category - API - get full info in categories list`, function (done) {
        request(url)
            .get('/api/category')
            .query('body=1&img=1')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function (res) {
                should.exist(res);
                res.body.should.matchEach(
                        value => value.should.have.keys([ '_id', 'body', 'name', 'parent', 'img' ])
                );
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            });
    });
    it(`DELETE /admin/category/:id - delete second category`, function (done) {
        agent
            .delete(`/admin/category/${idc[1]}`)
            .expect(function (res) {
                should.exist(res);
            })
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
    it(`DELETE /admin/category/:id - delete first category`, function (done) {
        agent
            .delete(`/admin/category/${idc[0]}`)
            .expect('Content-Type', /json/)
            .expect(function (res) {
                should.exist(res);
            })
            .expect(200, done)
    });
    it('GET /admin/logout - logout', function (done) {
        agent
            .get('/admin/logout')
            .expect('Content-Type', /plain/)
            .expect(302)
            .expect('Location', '/admin/login')
            .expect(/Redirecting/, done);
    });
});
describe('Comment', function () {
    this.timeout(1000);
    it('POST /api/comment - API - create comment', function (done) {
        request(url)
            .post('/api/comment')
            .send({'author': 'author test', 'body': 'test comment'})
            .expect(200)
            .expect(function (res) {
                should.exist(res);
                res.body.should.have.property('message');
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            });
    });
    it('GET /api/comment - API - get comment', function (done) {
        request(url)
            .get('/api/comment')
            .expect(200)
            .expect(function (res) {
                should.exist(res);
                res.body.should.have.property('count').which.is.a.Number();
                res.body.should.have.property('comments').which.is.a.Array();
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            });
    });
});
describe('Image manager', function () {
    this.timeout(1000);
    it('Should create a session', function (done) {
        agent
            .post('/admin/session')
            .send(_user)
            .expect(function (res) {
                if (!(res.res.statusCode == 201 || res.res.statusCode == 302)) {
                    throw new Error('Status code is')
                }
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            })
    });
    it('POST /admin/exhibit/image/dir - create dir', function (done) {
        agent
            .post('/admin/exhibit/image/dir')
            .send({'name': 'folder for test', 'path': './'})
            .expect(function (res) {
                res.body.should.have.property('message');
            })
            .expect(200, done);
    });
    it('POST /admin/exhibit/image - upload image', function (done) {
        agent
            .post('/admin/exhibit/image')
            .attach('globalImg', path.join(__dirname, 'files/test.JPG'))
            .field('path', './folder for test')
            .expect(function (res) {
                res.body.should.have.property('message');
            })
            .expect(200, done);
    });
    it('POST /admin/exhibit/image - invalid upload image ', function (done) {
        agent
            .post('/admin/exhibit/image')
            .attach('globalImg', path.join(__dirname, 'files/test.JPG'))
            .field('path', './folder for test')
            .expect(function (res) {
                res.body.should.have.property('message');
            })
            .expect(400, done);
    });
    it('PUT /admin/exhibit/image - rename images', function (done) {
        agent
            .put('/admin/exhibit/image')
            .send({'name': 'rename test.JPG', 'path': './folder for test/test.JPG'})
            .expect(function (res) {
                res.body.should.have.property('message');
            })
            .expect(200, done);
    });
    it('PUT /admin/exhibit/image/move - move images', function (done) {
        agent
            .put('/admin/exhibit/image/move')
            .send({'path': './folder for test/rename test.JPG', 'newPath': './'})
            .expect(function (res) {
                res.body.should.have.property('message');
            })
            .expect(200, done);
    });
    it('GET /admin/exhibit/image - content', function (done) {
        agent
            .get('/admin/exhibit/image')
            .query('path=./')
            .expect(200)
            .expect(function (res) {
                res.body.should.have.property('dir').which.is.a.String();
                res.body.should.have.property('list').which.is.a.Array();
            })
            .end(function (err, res) {
                if (err) return done(err);
                done();
            });
    });
    it('DELETE /admin/exhibit/image - delete image', function (done) {
        agent
            .delete('/admin/exhibit/image')
            .send({'path': './rename test.JPG'})
            .expect(200)
            .expect(function (res) {
                res.body.should.have.property('message');
            })
            .expect(200, done);
    });
    it('DELETE /admin/exhibit/image - delete folder', function (done) {
        agent
            .delete('/admin/exhibit/image')
            .send({'path': './folder for test'})
            .expect(200)
            .expect(function (res) {
                res.body.should.have.property('message');
            })
            .expect(200, done);
    });
    it('GET /admin/logout - logout', function (done) {
        agent
            .get('/admin/logout')
            .expect('Content-Type', /plain/)
            .expect(302)
            .expect('Location', '/admin/login')
            .expect(/Redirecting/, done);
    });
});
