'use strict';

const mongoose = require('mongoose');
const crypto = require('crypto');

const Schema = mongoose.Schema;

const AdminSchema = new Schema({
    adminname: {type: String, default: ''},
    email: {type: String},
    date: {type: Date, default: Date.now},
    salt: {type: String, default: ''},
    hashedPassword: {type: String, default: ''},
    resetPasswordToken: String,
    resetPasswordExpires: Date
});

const validatePresence = value => value && value.length;

AdminSchema
    .virtual('password')
    .set(function (password) {
        this._password = password;
        this.salt = this.makeSalt();
        this.hashedPassword = this.encryptPassword(password);
    })
    .get(function () {
        return this._password;
    });

AdminSchema.path('adminname').validate(function (adminname) {
    return adminname.length;
}, "Ім'я не повинно бути порожнім");

AdminSchema.path('adminname').validate(function (adminname, fn) {
    const Admin = mongoose.model('Admin');

    if (this.isNew || this.isModified('adminname')) {
        Admin.find({adminname}).exec(function (err, admins) {
            fn(!err && admins.length === 0);
        });
    } else fn(true);
}, "Ім'я вже використовується");

AdminSchema.path('email').validate(function (email, fn) {
    const Admin = mongoose.model('Admin');
    if (!this.isCreate && (this.isNew || this.isModified('email'))) {
        Admin.find({email: email}).exec(function (err, admins) {
            fn(!err && admins.length === 0);
        });
    } else fn(true);
}, 'Імеіл вже використовується');

AdminSchema.path('hashedPassword').validate(function (hashedPassword) {
    return hashedPassword.length && (!this.isModified('hashedPassword') || this._password.length);
}, 'Пароль не повинен бути порожнім');

AdminSchema.pre('save', function (next) {
    if (!this.isNew) return next();

    if (!validatePresence(this.password)) {
        next(new Error('Поганий пароль'));
    } else {
        next();
    }
});

AdminSchema.methods = {
    authenticate: function (parol) {
        return this.encryptPassword(parol) === this.hashedPassword;
    },
    makeSalt: function () {
        return Math.round((new Date().valueOf() * Math.random())) + '';
    },
    encryptPassword: function (password) {
        if (!password) return '';
        try {
            return crypto
                .createHmac('sha1', this.salt)
                .update(password)
                .digest('hex');
        } catch (err) {
            return '';
        }
    }
};

AdminSchema.statics = {
    load: function (options, cb) {
        options.select = options.select || 'adminname email hashedPassword salt';

        return this.findOne(options.criteria)
            .select(options.select)
            .exec(cb);
    },
    list: function (options, cb) {
        this.find(options).lean().exec(cb);
    }
};

mongoose.model('Admin', AdminSchema);