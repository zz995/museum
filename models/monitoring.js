'use strict';

const mongoose = require('mongoose');

exports.info = function (cb) {
    mongoose.connection.db.stats(function (err, stats) {
        if (err) return cb(err);

        const monitoring = new mongoose.mongo.Admin(mongoose.connection.db);
        monitoring.buildInfo(function (err, info) {
            if (err) return cb(err);
            cb(null, {
                version: info.version,
                maxSize: info.maxBsonObjectSize,
                collect: stats.collections,
                avgObjSize: stats.avgObjSize,
                dataSize: stats.dataSize,
                indexSize: stats.indexSize,
                indexes: stats.indexes
            });
        });
    });
};
