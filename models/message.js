'use strict';

const log = require('../libs/log').getLogger(module);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
    message: {type: String, trim: true},
    status: {type: Boolean},
    createdAt: {type: Date, default: Date.now, index: true},
    view: {type: Boolean, default: false},
    section: {type: String, trim: true},
    user: String,
    url: String
});


MessageSchema.methods = {
    saveMessage: function (cb) {
        const err = this.validateSync();
        if (err) {
            cb(err);
        } else {
            this.save(cb);
        }
    }
};

MessageSchema.statics = {
    list: function (options, needCount, cb) {
        let cursor = this.find(options.criteria);

        if (!needCount) {
            cursor
                .sort(options.sort)
                .limit(options.limit)
                .skip(options.limit * options.page)
                .exec(cb);
        } else {
            cursor.count(cb);
        }
    },

    updateMsg: function (ids, cb) {
        if (!ids) {
            cb(new Error('field is empty'));
        } else {
            this.update({_id: {$in: ids}}, {view: true}, {multi: true}, cb);
        }
    },

    destroy: function (criteria, cb) {
        this.find(criteria).remove().exec(cb);
    }

};

mongoose.model('Message', MessageSchema);