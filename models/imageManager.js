'use strict';

const path = require('path');
const rimraf = require('rimraf');
const fs = require('fs');
const multer = require('multer');
const config = require('../config');
const root = './globalImg';
const customStorage = require('../libs/storage');


exports.index = function (pathf, cb) {
    fs.readdir(pathf, function (err, item) {
        if (err) {
            return cb(err);
        }
        if (!item.length) {
            return cb(null, []);
        }

        const info = function (count, pathf, cb) {
            const response = [];
            let error = false;
            return item => fs.stat(path.join(pathf, item), (err, stats) => {
                if (error) return;

                if (err) {
                    error = true;
                    return cb(err);
                }

                response.push({
                    name: item,
                    size: stats.size,
                    mtime: stats.mtime.toLocaleString(),
                    isFile: stats.isFile()
                });

                if (!--count) {
                    cb(null, response);
                }
            })
        };

        item.map(info(item.length, pathf, cb));
    });
};

exports.destroy = function (pathf, cb) {
    rimraf(pathf, cb);
};

exports.rename = function (pathf, name, cb) {
    const newName = path.join(path.dirname(pathf), name);
    fs.stat(newName, function (err, stats) {
        if (err) {
            if (err.code === 'ENOENT') {
                fs.rename(pathf, newName, cb);
            } else {
                return cb(err);
            }
        } else {
            return cb({code: 'EPERM'});
        }
    });

};

exports.move = function (pathOld, pathNew, cb) {
    const newPath = path.join(pathNew, path.basename(pathOld));

    fs.stat(newPath, function (err, stats) {
        if (err) {
            if (err.code === 'ENOENT') {
                fs.rename(pathOld, newPath, cb);
            } else {
                return cb(err);
            }
        } else {
            return cb({code: 'EPERM'});
        }
    })

};

exports.createDir = function (pathf, name, cb) {
    fs.mkdir(path.join(pathf, name), cb);
};

exports.uploadImg = multer({
    storage: customStorage({
        destination: function (req, file, cb) {
            req.currentFileUpload =
                path.join(__dirname, '../public/', config.get('glImgUpload:temporaryFolder'), file.originalname);
            cb(null, req.currentFileUpload);
        }
    }),
    fileFilter: function (req, file, cb) {
        if (config.get('glImgUpload:supportMimeTypes').indexOf(file.mimetype) == -1) {
            cb(new Error(`Тип не підтримується ${file.mimetype}`), false);
        } else {
            cb(null, true);
        }
    },
    limits: config.get('glImgUpload:limits')
}).single('globalImg');
