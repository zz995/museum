'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const sortString = require('../libs/sortString');
const multer = require('multer');
const config = require('../config');
const path = require('path');
const crypto = require('crypto');
const Modify = require('../libs/modify.js');
const removeFiles = require('../libs/removeFiles').remove;
const mkdirp = require('mkdirp');
const customStorage = require('../libs/storage');

const museumSchema = new Schema({
    name: {type: String, trim: true},
    hidden: {type: Boolean, default: false},
    body: String,
    contact: {
        index: Number,
        address: String,
        phone: {
            type: [{type: String, trim: true}],
            set: phones => phones.split(',')
        },
        email: {
            type: [{type: String, trim: true}],
            set: emails => emails.split(',')
        }
    }
});

const CategorySchema = new Schema({
    parent: Schema.Types.ObjectId,
    name: {type: String, trim: true},
    nameIntl: {type: String, trim: true, index: true},
    charact: {
        type: [{type: String, trim: true}],
        set: tags => tags.split(',')
    },
    body: {
        type: String,
        required: true
    },
    img: {
        path: {type: String, trim: true, required: true},
        extname: {type: String, trim: true}
    },
    childs: [Schema.Types.ObjectId],
    museumInfo: {type: Boolean, default: false},
    aboutMuseum: museumSchema
});

const TransactionSchema = new Schema({
    source: Schema.Types.ObjectId,
    action: String,
    oldParent: Schema.Types.ObjectId,
    newParent: Schema.Types.ObjectId,
    nodeList: [Schema.Types.ObjectId],
    state: {type: String, default: 'initial'},
    img: {
        path: String,
        extname: String
    },
    lastModified: {type: Date, default: Date.now}
});

TransactionSchema.statics = {
    createTransct: function (expt, obj, cb) {
        this.collection.findAndModify(
            {action: {$in: expt}},
            null,
            {$setOnInsert: obj},
            {upsert: true},
            (err, doc) => {
                if (err) return cb(err);
                if (!doc.lastErrorObject.updatedExisting) {
                    Transact.findOne({_id: doc.lastErrorObject.upserted}).exec(cb);
                } else {
                    cb({message: 'В даний момент не можливо виконати', code: 'CONFLICT'});
                }
            }
        );
    }
};

const Transact = mongoose.model('Transaction', TransactionSchema);

CategorySchema.methods = {
    saveCategory: function (cb) {
        const err = this.validateSync();
        if (err) {
            cb(err);
        } else {
            this.save(cb);
        }
    },

    saveCategoryUpdateParent: function (cb) {
        const err = this.validateSync();
        if (err) return cb({message: err.message, codeErrorCategory: 400});
        if (this.parent === null) return this.saveCategory(cb);
        Transact.createTransct(
            ['move', 'delete'],
            {
                newParent: this.parent,
                source: this._id,
                action: 'create',
                state: 'initial create',
                img: {
                    path: this.img.path,
                    extname: this.img.extname
                }
            },
            (err, transac) => {
                if (err) {
                    return cb(err);
                }
                this.constructor.find({_id: this.parent}).count().exec((err, count) => {
                    if (err) return cb(err);
                    if (count) {
                        return this.createCat(transac, cb);
                    } else {
                        const error = new Error('Неможливо видалити, міститить підкатегорії');
                        error.code = 'CONFLICT';
                        return this._remove(
                            transac,
                            error,
                            cb
                        );
                    }
                });
            }
        );
    },

    deleteCategoryUpdateParent: function (cb) {
        Transact.createTransct(
            ['move', 'create'],
            {
                newParent: this.parent,
                source: this._id,
                action: 'delete',
                state: 'initial delete'
            },
            (err, transac) => {
                if (err) return cb(err);
                this.constructor.leaf(this._id, (err, bool) => {
                    if (err) return cb(err);
                    if (bool) {
                        return this.deleteCat(transac, cb);
                    } else {
                        const error = new Error('Неможливо видалити, міститить підкатегорії');
                        error.code = 'CONFLICT';
                        return this._remove(
                            transac,
                            error,
                            cb
                        );
                    }
                });
            }
        );
    },

    move: function (cb) {
        Transact.createTransct(
            ['move', 'create', 'delete'],
            {
                oldParent: this.oldIdPar,
                newParent: this.parent,
                source: this._id,
                action: 'move'
            },
            (err, transac) => {
                if (err) return cb(err);
                return this.moveApply(transac, cb);
            }
        );
    },

    _errorDB: function (err, msg) {
        err.message += msg;
        throw err;
    },

    _remove: function (tr, data, cb) {
        tr.remove(err => {
            if (err) this._errorDB(err, ' (Невдалося видалити транзакцію)');
            return cb(data);
        });
    },

    _trSetState: function (tr, state, cb) {
        tr.state = state;
        tr.save(err => {
            if (err) {
                this._errorDB(err, ` (У транзакції неможливо встановити новий стан ${state})`);
            } else {
                if (tr.action == 'move') {
                    this.moveApply(tr, cb);
                } else {
                    this.createCat(tr, cb);
                }
            }
        });
    },

    createCat: function (tr, cb) {
        const remove = ((tr, cb) =>
            (data) => this._remove(tr, data, cb))(tr, cb);
        const trSetState = ((tr, cb) =>
            (state) =>  this._trSetState(tr, state, cb))(tr, cb);

        if (tr.state == 'initial create') {
            this.save(err => {
                if (err) {
                    remove(err);
                } else {
                    trSetState('applied save');
                }
            });
        } else if (tr.state == 'applied save') {
            if (this.parent) {
                this.constructor.update(
                    {$or: [{childs: tr.newParent}, {_id: tr.newParent}]},
                    {$addToSet: {childs: tr.source}},
                    {multi: true},
                        err => {
                        if (err) {
                            trSetState('initial delete');
                        } else {
                            remove();
                        }
                    }
                );
            } else {
                remove();
            }
        } else {
            this.deleteCat(tr, cb);
        }
    },

    deleteCat: function (tr, cb) {
        const trSetState = ((tr, cb) =>
            (state) =>  this._trSetState(tr, state, cb))(tr, cb);
        const errorDB = this._errorDB;
        const remove = ((tr, cb) =>
            (data) => this._remove(tr, data, cb))(tr, cb);

        if (tr.state == 'initial delete') {
            if (this.parent) {
                this.constructor.update(
                    {childs: tr.source},
                    {$pull: {childs: tr.source}},
                    {multi: true},
                        err => {
                        if (err) {
                            if (tr.action == 'delete') {
                                cb(new Error('Неможливо здійснити видалення'));
                            } else {
                                errorDB(err, ' (Неможливо відновитися після відміни транзакції)');
                            }
                        } else {
                            trSetState('applied delete');
                        }
                    }
                );
            } else {
                trSetState('applied delete');
            }

        } else if (tr.state == 'applied delete') {
            this.remove(err => {
                if (err) {
                    if (tr.action == 'delete') {
                        errorDB(err, ' (Неможливо здійснити видалення)');
                    } else {
                        errorDB(err, ' (Неможливо відновитися після відміни транзакції)');
                    }
                } else {
                    remove(tr.action == 'delete'
                            ? null
                            : new Error('Невдалося виконати запит')
                    );
                }
            });
        }
    },

    destroyImg: function (cb) {
        const listFile = [];
        const img = this.img;
        const transformSize = config.get('categoryImg:transformSize');
        const extname = config.get('categoryImg:formatTransform');
        const fullPath = path.join(__dirname, '../public/', img.path);

        for (let i = 0, l = transformSize.length; i < l; i++) {
            listFile.push(fullPath + '_' + transformSize[i].pre + '.' + extname);
        }

        listFile.push(path.join(__dirname, '../public', img.path + img.extname));
        listFile.map(removeFiles(listFile.length, function (msg) {
            cb(!msg.success, msg.message);
        }));
    },

    moveApply: function moveApply(tr, cb) {
        const trSetState = ((tr, cb) =>
            (state) =>  this._trSetState(tr, state, cb))(tr, cb);
        const remove = ((tr, cb) =>
            (data) => this._remove(tr, data, cb))(tr, cb);

        if (tr.state == 'initial') {
            const list = [];
            for (let i = 0, l = this.childs.length; i < l; i++) {
                list.push(this.childs[i]);
            }
            list.push(this.id);
            tr.nodeList = list;
            trSetState('pending');

        } else if (tr.state == 'pending') {
            this.constructor.update(
                {$or: [{childs: tr.oldParent}, {_id: tr.oldParent}]},
                {$pull: {childs: {$in: tr.nodeList}}},
                {multi: true},
                    err => {
                    if (err) {
                        trSetState('canceling pull');
                    } else {
                        trSetState('applied pull');
                    }
                });
        } else if (tr.state == 'applied pull') {
            this.constructor.update(
                {$or: [{childs: tr.newParent}, {_id: tr.newParent}]},
                {$addToSet: {childs: {$each: tr.nodeList}}},
                {multi: true},
                    err => {
                    if (err) {
                        trSetState('canceling set');
                    } else {
                        trSetState('applied set');
                    }
                });
        } else if (tr.state == 'applied set') {
            this.parent = tr.newParent;
            this.save(err => {
                if (err) {
                    trSetState('canceling set');
                } else {
                    remove();
                }
            });
        } else {
            this.moveCancel(tr, cb);
        }
    },

    moveCancel: function (tr, cb) {
        const trSetState = ((tr, cb) =>
            (state) =>  this._trSetState(tr, state, cb))(tr, cb);
        const errorDB = this._errorDB;
        const remove = ((tr, cb) =>
            (data) => this._remove(tr, data, cb))(tr, cb);

        if (tr.state == 'canceling pull') {
            this.constructor.update(
                {$or: [{childs: tr.oldParent}, {_id: tr.oldParent}]},
                {$addToSet: {childs: {$each: tr.nodeList}}},
                {multi: true},
                    err => {
                    if (err) {
                        return errorDB(err, ' (Неможливо відновитися після відміни транзакції)');
                    }
                    remove(new Error('Невдалося перемістити категорію'));
                }
            );
        } else if (tr.state == 'canceling set') {
            this.constructor.update(
                {$or: [{childs: tr.newParent}, {_id: tr.newParent}]},
                {$pull: {childs: {$in: tr.nodeList}}},
                {multi: true},
                    err => {
                    if (err) return errorDB(err, ' (Неможливо відновитися після відміни транзакції)');
                    trSetState('canceling pull');
                }
            );
        }
    }
};

CategorySchema.virtual('title').set(function (title) {
    this.name = title;
    this.nameIntl = sortString(this.name);
});

CategorySchema.statics = {
    load: function (_id, cb) {
        return this.findOne({_id}).exec(cb);
    },

    list: function (opt, cb) {
        opt['name'] = 1;
        opt['parent'] = 1;
        this.find({}, opt).lean().sort({nameIntl: 1}).exec(cb);
    },

    leaf: function (id, cb) {
        this.findOne({parent: id}).exec(function (err, doc) {
            if (err) {
                cb(err);
            } else {
                cb(err, !doc);
            }
        })
    },

    upload: multer({
        storage: customStorage({
            destination: function (req, file, cb) {
                const hash = crypto.createHash('md5')
                    .update(file.originalname + (new Date()).getTime().toString())
                    .digest('hex');
                let relativePath = path.join(
                    '/uploads',
                    file.mimetype.split('/')[0],
                    hash.slice(0, 1),
                    hash.slice(1, 2)
                );
                let nameFile = hash;
                const pathFile = path.join(
                    __dirname,
                    '../public',
                    relativePath
                );
                file.relativepath = path.join(relativePath, nameFile);
                mkdirp(pathFile, function (err) {
                    if (err) throw new Error(err);
                    else {
                        file.changeName = nameFile;
                        file.extname = path.extname(file.originalname);
                        req.currentFileUpload = path.join(pathFile, nameFile + file.extname);
                        cb(null, req.currentFileUpload);
                    }
                });
            }
        }),
        fileFilter: function (req, file, cb) {
            if (config.get('categoryImg:supportMimeTypes').indexOf(file.mimetype) == -1) {
                const err = new Error(`Тип не підтримується ${file.mimetype}`);
                err.code = 'UNSUPPORTED_MEDIA_TYPE';
                cb(err, false);
            } else {
                cb(null, true);
            }
        },
        limits: config.get('categoryImg:limits')
    }).single('categoryImg'),

    resize: function (req, cb) {
        const file = {
            name: req.file.path,
            orgName: req.file.originalname
        };

        const prefix = config.get('categoryImg:transformSize');
        const quality = config.get('categoryImg:quality');
        var chSize = new Modify(
            [file], prefix,
            config.get('categoryImg:concurrency'),
            config.get('categoryImg:formatTransform'),
            quality
        );

        chSize.start(function (err) {
            const set = new Set();
            const listFile = [];

            for (let i = 0; i < err.length; i++) {
                set.add(err[i].name);
            }

            set.forEach(value => {
                for (let i = 0, l = prefix.length; i < l; i++) {
                    listFile.push(`${value}_${prefix[i].pre}.jpg`);
                }
            });

            if (!config.get('categoryImg:saveOrgImg')) {
                listFile.push(file.name);
            }

            if (!listFile.length) {
                cb(null);
            } else {
                listFile.map(removeFiles(listFile.length, msg => {
                    let resizeErr = '';
                    for (let i = 0; i < err.length; i++) {
                        resizeErr += err.msg;
                        resizeErr += ` (${err.orgName} => ${err.nextName})\n`;
                    }
                    const response = {
                        resizeErr,
                        unlinkMsg: msg
                    };

                    cb(response);
                }));
            }
        })
    },

    destroyImg: function (listFile, cb) {
        listFile.map(removeFiles(listFile.length, cb));
    },

    restore: function (cb) {
        Transact.findOne({}).exec((err, transac) => {
            if (err) return cb(err);
            if (transac) {
                this.findOne({_id: transac.source}).exec((err, category) => {
                    if (err) return cb(err);
                    if (!category && transac.action == 'delete') return transac.remove(err => {
                        if (err) return cb(err);
                        this.restore(cb);
                    });
                    const callback = this.restore.bind(this, cb);
                    if (transac.action == 'move') {
                        category.moveApply(transac, callback);
                    } else {
                        if (transac.action == 'create') {
                            if (category) {
                                category.destroyImg(err => {
                                    if (err) return cb(err);
                                    transac.action = 'delete';
                                    category._trSetState(transac, 'initial delete', callback)
                                });
                            } else {
                                const listFile = [];
                                const img = transac.img.path;
                                const transformSize = config.get('categoryImg:transformSize');
                                listFile.push(path.join(__dirname, '../public', img + transac.img.extname));
                                for (let i = 0, l = transformSize.length; i < l; i++) {
                                    listFile.push(path.join(__dirname, '../public', `${img}_${transformSize[i].pre}.jpg`));
                                }
                                this.destroyImg(listFile, msg => {
                                    if (!msg.success) return cb(err);
                                    return transac.remove(err => {
                                        if (err) return cb(err);
                                        this.restore(cb);
                                    });
                                })
                            }
                        } else {
                            category.createCat(transac, callback);
                        }
                    }
                });
            } else {
                cb();
            }
        });
    }
};

mongoose.model('Category', CategorySchema);