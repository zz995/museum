'use strict';

const mongoose = require('mongoose');
const ObjectID = require('mongodb').ObjectID;
const sortString = require('../libs/sortString');
const multer = require('multer');
const config = require('../config');
const imgSupType = config.get('upload:img:supportMimeTypes');
const docSupType = config.get('upload:doc:supportMimeTypes');
const Modify = require('../libs/modify.js');
const removeFiles = require('../libs/removeFiles').remove;
const customStorage = require('../libs/storage');
const fs = require('fs');

const crypto = require('crypto');
const path = require('path');
const mkdirp = require('mkdirp');
const Schema = mongoose.Schema;

const getTags = tags => tags.join(', ');
const setTags = tags => tags.split(',');

const Img = new Schema({
    relativepath: {type: String, trim: true, required: true},
    originalname: {type: String, trim: true, default: ''}
});

const Files = new Schema({
    relativepath: {type: String, trim: true, required: true},
    originalname: {type: String, trim: true, default: ''},
    description: {type: String}
});

const ExhibitSchema = new Schema({
    title: {type: String, trim: true, required: true, index: true},
    titleIntl: {type: String, trim: true, required: true, index: true},
    hidden: {type: Boolean, default: false},
    createdAt: {type: Date, default: Date.now, index: true},
    img: {type: Img, required: true},
    inventoryNumber: {type: String, trim: true},
    museumNumber: {type: String, trim: true},
    dateStarted: {type: Number, index: true},
    dateFinish: {type: Number, index: true},
    categories: [{type: Schema.Types.ObjectId, ref: 'Category'}],
    bodyShort: String,
    body: String,
    tags: {
        type: [{type: String, trim: true}],
        index: true,
        get: getTags,
        set: setTags
    },
    characteristics: [{
        name: {type: String, trim: true, required: true},
        value: {type: String, trim: true, required: true}
    }],
    mediaCDN: [{
        type: {type: String, trim: true, required: true},
        link: {type: String, trim: true, required: true},
        description: String
    }],
    images: [Files],
    docs: [Files],
    model3d: {
        bin: {type: String, trim: true},
        json: {type: String, trim: true}
    },
    altModel3d: {
        count: Number,
        relativepath: {type: String, trim: true}
    }
});

ExhibitSchema.index({allCategories: 1, titleIntl: 1});
ExhibitSchema.index({'characteristics.name': 1, 'characteristics.value': 1});

ExhibitSchema.methods = {
    saveExhibit: function (cb) {
        const err = this.validateSync();
        if (err) {
            cb(err);
        } else {
            this.save(err => {
                if (err) return cb(err);
                this.model(this.constructor.modelName).tags(function (err) {
                    if (err) return cb(null, true);
                    cb();
                });
            });
        }
    },

    prepareDelete: function (needDel) {
        const img = this.img;
        const listFile = [];
        const transformSize = config.get('upload:img:transformSize');
        const extname = config.get('upload:img:formatTransform');
        const pathDirPublic = path.join(__dirname, '../public');

        const stayDB = {};

        if (!needDel || needDel.delImg) {
            const imgs = this.images;
            let delImg = null;
            if (needDel) {
                stayDB.images = [];
                delImg = needDel.delImg;
            }
            for (let i = 0; i < imgs.length; i++) {
                const pathImg = path.join(pathDirPublic, imgs[i].relativepath);
                if (!needDel || delImg[path.basename(pathImg)] === 'on') {
                    listFile.push(pathImg);
                    for (let j = 0, l = transformSize.length; j < l; j++) {
                        listFile.push(pathImg + '_' + transformSize[j].pre + '.' + extname);
                    }
                } else {
                    stayDB.images && stayDB.images.push(i)
                }
            }
        }

        if (!needDel || needDel.delDoc) {
            const docs = this.docs;
            let delDoc = null;
            if (needDel) {
                stayDB.docs = [];
                delDoc = needDel.delDoc;
            }
            for (let i = 0; i < docs.length; i++) {
                const pathDoc = path.join(pathDirPublic, docs[i].relativepath);
                if (!needDel || delDoc[path.basename(pathDoc)] === 'on') {
                    listFile.push(pathDoc);
                } else {
                    stayDB.docs && stayDB.docs.push(i)
                }
            }
        }

        if (!needDel || needDel.img) {
            const pathImg = path.join(pathDirPublic, img.relativepath);
            listFile.push(pathImg);
            for (let i = 0, l = transformSize.length; i < l; i++) {
                listFile.push(pathImg + '_' + transformSize[i].pre + '.' + extname);
            }
        }


        let path3d = '';
        if (!needDel || (needDel.model3dJson && needDel.model3dBin) || needDel.model3dDelete) {
            const mod3d = this.model3d;
            if (mod3d) {
                if (mod3d.bin) {
                    path3d = path.join(pathDirPublic, mod3d.bin);
                    listFile.push(path3d);
                }
                if (mod3d.json) {
                    path3d = path.join(pathDirPublic, mod3d.json);
                    listFile.push(path3d);
                }
            }
        }

        if (!needDel || needDel.altModel3d || needDel.altModel3dDelete) {
            const altModel = this.altModel3d;
            if (altModel && altModel.relativepath) {
                listFile.push(path.join(pathDirPublic, altModel.relativepath));
            }
        }

        return {path3d, stayDB, listFile};
    },

    destroyFile: function (prepareObj, cb) {

        prepareObj = prepareObj || this.prepareDelete();
        let path3d = prepareObj.path3d, listFile = prepareObj.listFile;

        listFile.map(removeFiles(listFile.length, function (msg) {
            if (path3d) {
                const pathDir = path.dirname(path3d);
                fs.rmdir(pathDir, function (err) {
                    if (err) {
                        msg.message += err.message + '\n';
                    } else {
                        msg.message += 'successfully deleted folder ' + pathDir + '\n';
                    }
                    cb(!msg.success, msg.message);
                });

            } else {
                cb(!msg.success, msg.message);
            }
        }));
    }
};

ExhibitSchema.virtual('name').set(function (title) {
    this.title = title;
    this.titleIntl = sortString(title);
});

ExhibitSchema.virtual('obDate').set(function (y) {
    if (y.start)
        this.dateStarted = y.start.y + '-' + y.start.m + '-' + y.start.d;
    if (y.finish)
        this.dateFinish = y.finish.y + '-' + y.finish.m + '-' + y.finish.d;
});

ExhibitSchema.statics = {
    load: function (_id, cb) {
        return this.findOne({_id}, {titleIntl: 0, allCategories: 0}).populate('categories', 'name').exec(cb);
    },

    list: function (options, needCount, cb) {
        const select = {
            bodyShort: 1,
            title: 1,
            dateStarted: 1,
            dateFinish: 1,
            tags: 1,
            categories: 1,
            img: 1,
            createdAt: 1
        };
        if (options.isAdmin) select.hidden = 1;

        const cursor = this
            .find(options.criteria, select)
            .lean();

        if (!needCount) {
            cursor
                .populate('categories', 'name')
                .sort(options.sort)
                .limit(options.limit)
                .skip(options.limit * options.page)
                .exec(cb);
        } else {
            cursor.count(cb);
        }
    },

    used: function (id, cb) {
        this.findOne({categories: id}).exec(function (err, doc) {
            if (err) {
                cb(err);
            }
            cb(err, !doc);
        });
    },

    upload: function () {
        let model3dRelativePath = '';

        multer({
            storage: customStorage({
                destination: function (req, file, cb) {
                    const hash = crypto.createHash('md5')
                        .update(file.originalname + (new Date()).getTime().toString())
                        .digest('hex');
                    let relativePath = path.join(
                        '/uploads',
                        file.mimetype.split('/')[0],
                        hash.slice(0, 1),
                        hash.slice(1, 2)
                    );

                    let nameFile = '';
                    if (file.fieldname == 'docs' || file.fieldname == 'altModel3d') {
                        nameFile = `${hash}${path.extname(file.originalname)}`;
                    } else if (file.fieldname == 'model3dBin' || file.fieldname == 'model3dJson') {
                        nameFile = file.originalname;
                        model3dRelativePath = model3dRelativePath || path.join(relativePath, hash);
                        relativePath = model3dRelativePath;
                    } else {
                        nameFile = hash;
                    }

                    const pathFile = path.join(
                        __dirname,
                        '../public',
                        relativePath
                    );
                    file.relativepath = path.join(relativePath, nameFile);
                    mkdirp(pathFile, function (err) {
                        if (err) throw new Error(err);
                        else {
                            file.changeName = nameFile;
                            req.currentFileUpload = path.join(pathFile, nameFile);
                            req.currentFileUploadIs3d = file.fieldname == 'model3dBin' || file.fieldname == 'model3dJson';

                            cb(null, path.join(pathFile, nameFile));
                        }
                    });
                }
            }),
            fileFilter: function (req, file, cb) {
                if ((file.fieldname == 'model3dBin' || file.fieldname == 'model3dJson')
                    && file.mimetype !== 'application/octet-stream') {
                    return cb(null, false);
                }

                if (((file.fieldname == 'images' || file.fieldname == 'mainPicture' || file.fieldname == 'altModel3d')
                    && imgSupType.indexOf(file.mimetype) == -1)
                    || (file.fieldname == 'docs' && docSupType.indexOf(file.mimetype) == -1)) {

                    if (!req.errorMessageDB) {
                        req.errorMessageDB = '';
                    }
                    req.errorMessageDB += `File ${file.originalname} in fieldname ${file.fieldname} do not supported\n`;

                    cb(null, false);
                } else {
                    cb(null, true);
                }
            }
        }).fields([
            {name: 'mainPicture', maxCount: 1},
            {name: 'images'},
            {name: 'docs'},
            {name: 'model3dBin', maxCount: 1},
            {name: 'model3dJson', maxCount: 1},
            {name: 'altModel3d', maxCount: 1}
        ]).apply(this, arguments);
    },

    resize: function (req, cb) {
        const mainImg = req.files.mainPicture;
        const files = [];

        const imgs = req.files.images;
        if (imgs) {
            for (let i = 0; i < imgs.length; i++) {
                files.push({
                    name: imgs[i].path,
                    orgName: imgs[i].originalname
                })
            }
        }

        if (mainImg) {
            files.push({
                name: mainImg[0].path,
                orgName: mainImg[0].originalname
            });
        }

        const prefix = config.get('upload:img:transformSize');
        const quality = config.get('upload:img:quality');
        const chSize = new Modify(
            files, prefix,
            config.get('upload:img:concurrency'),
            config.get('upload:img:formatTransform'),
            quality
        );

        chSize.start(function (err) {
            const set = new Set();
            const listFile = [];

            for (let i = 0; i < err.length; i++) {
                set.add(err[i].name);
            }

            set.forEach(value => {
                for (let i = 0, l = prefix.length; i < l; i++) {
                    listFile.push(`${value}_${prefix[i].pre}.jpg`);
                }
            });

            for (let i = 0; i < files.length; i++) {
                listFile.push(files[i].name);
            }

            listFile.map(removeFiles(listFile.length, msg => {
                let resizeErr = '';
                for (let i = 0; i < err.length; i++) {
                    resizeErr += err.msg;
                    resizeErr += ` (${err.orgName} => ${err.nextName})\n`;
                }
                const response = {
                    resizeErr,
                    unlinkMsg: msg
                };

                cb(response);
            }));
        })

    },

    destroyFile: function (listFile, cb) {
        listFile.map(removeFiles(listFile.length, cb));
    },

    tags: function (cb) {
        this.aggregate([
            {$project: {tags: 1, _id: 0}},
            {$unwind: "$tags"},
            {$match: {tags: {$ne: ''}}},
            {
                $group: {
                    _id: '$tags',
                    count: {$sum: 1}
                }
            },
            {$sort: {count: -1}},
            {$limit: config.get('tags:maxCount')}
        ], function (err, doc) {
            if (err) return cb(err);
            global.tags = doc;
            cb();
        });
    }
};

mongoose.model('Exhibit', ExhibitSchema);