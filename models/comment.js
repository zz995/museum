'use strict';

const mongoose = require('mongoose');
const ObjectID = require('mongodb').ObjectID;
const Schema = mongoose.Schema;

const CommentSchema = new Schema({
    createdAt: {type: Date, default: Date.now, index: true},
    author: {type: String, trim: true, default: 'Невідомий'},
    body: String,
    visible: {type: Boolean, default: false}
});

CommentSchema.methods = {
    saveComment: function (cb) {
        const err = this.validateSync();
        if (err) {
            cb(err);
        } else {
            this.save(cb);
        }
    }
};

CommentSchema.statics = {
    load: function (_id, cb) {
        return this.findOne({_id}).exec(cb);
    },

    list: function (options, needCount, visibleNeed, cb) {
        const cursor = visibleNeed
            ? this.find(options.criteria)
            : this.find(options.criteria, {createdAt: 1, author: 1, body: 1})
            .lean();

        if (!needCount) {
            cursor
                .sort(options.sort)
                .limit(options.limit)
                .skip(options.limit * options.page)
                .exec(cb);
        } else {
            cursor.count(cb);
        }
    }
};

mongoose.model('Comment', CommentSchema);