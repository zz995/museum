'use strict';

const winston = require('winston');
const config = require('../config');

const formatter = new Intl.DateTimeFormat("uk-UA", {
    hour: "numeric",
    minute: "numeric",
    second: "numeric",
    day: "numeric",
    month: "numeric",
    year: "numeric"
});

exports.getLogger = function (module) {
    const path = module.filename.split('/').slice(-2).join('/');

    return new winston.Logger({
        transports: [
            process.env.NODE_ENV === 'development'
            ? new winston.transports.Console({
                colorize: true,
                level: 'debug',
                label: path
            })
            : new winston.transports.File({
                level: 'error',
                handleExceptions: true,
                filename: `log/error.log`,
                json: true,
                label: path,
                maxsize: 1048576,
                maxFiles: 5,
                timestamp: () => formatter.format(new Date())
            })
        ]
    });
};

exports.getLogActions = function () {
    return new winston.Logger({
        transports: [
            new winston.transports.File({
                level: 'info',
                filename: 'log/actions.log',
                maxsize: 1048576,
                maxFiles: 5,
                timestamp: () => formatter.format(new Date())
            })
        ]
    })
};