'use strict';

module.exports = function (str) {
    const alf = 'абвгґдеєёжзиіїйклмнопрстуфхцчшщъыьэюя';
    const st = 'абвгдежзийклмнопрстуфхцчшщъыьэюяёєіїґ';
    let map = new Map();

    for (let i = 0, l = alf.length; i < l; i++) {
        map.set(alf[i], st[i]);
    }

    str = str.toLowerCase();
    return str.split('').reduce((str2, item) => str2 += map.get(item) || item, '');
};