'use strict';

const mongoose = require('mongoose');
const Admin = mongoose.model('Admin');

module.exports = function (cb) {
    Admin.count(function (err, count) {
        if (err) cb(err);
        if (!count) {
            const admin = new Admin({adminname: 'root', password: 'root'});
            admin.save(function (err) {
                if (err) cb(err);
                cb();
            })
        } else {
            cb();
        }
    });
};

