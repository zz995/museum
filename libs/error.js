'use strict';

const path = require('path');
const util = require('util');
const http = require('http');

HttpError.STATUS_CODES = new Map()
    .set(200, 'Добре')
    .set(400, 'Невірний запит')
    .set(401, 'Не авторизований')
    .set(403, 'Забороненно')
    .set(404, 'Не знайденно')
    .set(409, 'Конфлікт')
    .set(413, 'Розмір занадто великий')
    .set(415, 'Тип даних не підтримується')
    .set(500, 'Внутрішня помилка сервера')
    .set(501, 'Не реалізовано')
    .set(503, 'Недоступно');

function HttpError(status, msg) {
    Error.apply(this, arguments);
    Error.captureStackTrace(this, HttpError);

    this.status = status;
    this.message = msg || HttpError.STATUS_CODES.get(status) || http.STATUS_CODES[status] || "Помилка";
}

util.inherits(HttpError, Error);

HttpError.prototype.name = 'HttpError';

exports.HttpError = HttpError;