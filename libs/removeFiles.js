'use strict';

const fs = require('fs');
const path = require('path');

exports.remove = function (count, cb) {
    let i = 0;
    let success = true;
    let message = '';
    return function (fileName) {
        fs.unlink(fileName, function (err) {
            if (err && err.code !== 'ENOENT') {
                success = false;
                message += err.message + '\n';
            } else {
                if (err && err.code === 'ENOENT') {
                    message += 'not found ' + fileName + '\n';
                } else {
                    message += 'successfully deleted ' + fileName + '\n';
                }
                success = success && true;
            }

            i += 1;
            if (i == count) {
                cb({message, success});
            }
        });
    }
};
