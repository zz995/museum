'use strict';

const gm = require('gm');
const fs = require('fs');
const path = require('path');

function Modify(imgs, opt, concurrency, format, compress) {
    this.format = format || 'jpg';
    this.compress = compress || 60;
    this.imgs = imgs;
    this.opt = opt;
    this.concurrency = concurrency || 2;
    this.running = 0;
    this.completed = 0;
    this.cursor = 0;
    this.error = [];
    this.currentOpt = 0;
    this.taskLen = this.opt.length * this.imgs.length;
}

Modify.prototype.createStreamGm = function () {
    this.running++;
    const pre = this.opt[this.currentOpt].pre;
    const size = this.opt[this.currentOpt].size;
    const idn = this.cursor;
    const img = this.imgs[idn];
    let chunks = [];
    let name = img.name;

    const indExt = name.indexOf('.');
    if (indExt + 1) {
        name = name.slice(0, indExt);
    }

    name = `${name}_${pre}.${this.format}`;

    const close = () => {
        this.completed++;
        this.running--;
        const err = chunks.join('');

        if (err) {
            this.error.push({
                msg: err,
                name: img.name,
                orgName: img.orgName,
                nextName: name,
                pre: pre
            });
        }

        if (this.completed == this.taskLen) {
            this.cb(this.error);
        } else {
            if (this.concurrency) {
                if ((this.running + this.completed) < this.taskLen) {
                    this.createStreamGm();
                }
            }
        }
    };

    gm(img.name)
        .noProfile()
        .resize(size)
        .quality(this.compress)
        .stream(this.format, (err, stdout, stderr) => {
            if (err) {
                chunks.push(err.message);
                close();
            } else {
                stdout.on('close', close);

                var writeStream = fs.createWriteStream(name);
                stdout.pipe(writeStream);
                stderr.on('data', chunk => {
                    chunks.push(chunk);
                });
            }

        });

    const count = (this.currentOpt + 1) % this.opt.length;
    this.cursor += count ? 0 : 1;
    this.currentOpt = count;

};

Modify.prototype.start = function (cb) {
    this.cb = cb;
    if (this.concurrency) {
        let i = 0;
        for (; i < this.concurrency && i < this.taskLen; i++) {
            this.createStreamGm();
        }
    } else {
        for (let i = 0; i < this.taskLen; i++) {
            this.createStreamGm();
        }
    }
};

module.exports = Modify;