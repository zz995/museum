'use strict';

const mongoose = require('mongoose');
const LocalStrategy = require('passport-local').Strategy;
const Admin = mongoose.model('Admin');

module.exports = new LocalStrategy({
        usernameField: 'adminname',
        passwordField: 'password'
    },
    
    function (adminname, password, done) {
        const options = {
            criteria: {adminname: adminname}
        };
        Admin.load(options, function (err, admin) {
            if (err) return done(err);
            if (!admin) {
                return done(null, false, {message: 'Ім\'я або пароль не вірний'});
            }
            if (!admin.authenticate(password)) {
                return done(null, false, {message: 'Ім\'я або пароль не вірний'});
            }
            return done(null, admin);
        });
    }
);