'use strict';

const mongoose = require('mongoose');
const ObjectID = mongoose.Types.ObjectId;
const Admin = mongoose.model('Admin');
const crypto = require('crypto');
const Message = mongoose.model('Message');
const HttpError = require('../libs/error').HttpError;

exports.load = function (req, res, next, _id) {
    try {
        _id = new ObjectID(_id);
    } catch (e) {
        return next(404);
    }
    const options = {
        criteria: {_id}
    };
    Admin.load(options, function (err, admin) {
        if (err) return next(new HttpError(500, err.message));
        if (!admin) {
            return next(404);
        }
        req.admin = admin;
        next();
    })
};

exports.create = function (req, res, next) {
    const admin = new Admin(req.body);
    admin.isCreate = true;

    admin.save(function (err) {
        if (err) {
            const errors = err.errors;
            let messages = '';
            for (let item in errors) {
                if (!errors.hasOwnProperty(item)) continue;
                messages += errors[item].message + '\n';
            }
            req.flash('error', messages);
            return res.redirect(req.originalUrl);
        }
        req.flash('success', 'Адміністратор створений: ' + admin.adminname);
        return res.redirect(req.originalUrl);
    });
};

exports.login = function (req, res) {
    res.sendDataClient({
        title: 'Авторизація',
        error: req.flash('error')
    }, 'admin/login');

};

exports.signup = function (req, res) {
    res.sendDataClient({
        title: 'Реєстрація',
        admin: new Admin(),
        error: req.flash('error')
    }, 'admin/signup');
};

exports.logout = function (req, res) {
    req.logout();
    res.redirect('/admin/login');
};

exports.session = function (req, res) {
    const redirectTo = req.session.returnTo
        ? req.session.returnTo
        : '/admin';
    delete req.session.returnTo;
    res.redirect(redirectTo);
};

exports.forgot = function (req, res) {
    res.sendDataClient({
        title: 'Відновлення',
        error: req.flash('error'),
        success: req.flash('success')
    }, 'admin/forgot');
};

const nodemailer = require('nodemailer');

exports.recovery = function (req, res, next) {
    crypto.randomBytes(20, function (err, buf) {
        var token = buf.toString('hex');
        if (err) return next(new HttpError(500, err.message));
        const options = {
            criteria: {email: req.body.email}
        };

        Admin.load(options, function (err, admin) {
            if (err) return next(new HttpError(500, err.message));
            if (!admin) {
                req.flash('error', 'За даним імейлом адміністратор не знайдений');
                return res.redirect('/admin/forgot');
            }
            admin.resetPasswordToken = token;
            admin.resetPasswordExpires = Date.now() + 3600000; // 1 hour

            admin.save(function (err) {
                if (err) return next(new HttpError(500, err.message));
                const transporter = nodemailer.createTransport({
                    service: 'Gmail',
                    auth: {
                        user: 'museum995192@gmail.com',
                        pass: 'ctrhtnysqgfhjkm'
                    }
                });

                var mailOptions = {
                    from: 'museum995192@gmail.com',
                    to: admin.email,
                    subject: 'Зміна пароля',
                    html: '<p>Для того щоб відновити пароль необхідно перейти за наступним <b>посиланням:</b></p>'
                    + '<p>http://' + req.headers.host + '/admin/reset/' + token + '<br><br>'
                    + 'Якщо не перейти, пароль залишеться незміненим'
                };

                transporter.sendMail(mailOptions, function (err, info) {
                    if (err) {
                        req.flash('error', err.message);
                    }

                    req.flash('success', 'Імейл відправлений: ' + admin.email);
                    return res.redirect('/admin/forgot');
                });
            });
        });
    });
};

exports.reset = function (req, res, next) {
    const options = {
        criteria: {
            resetPasswordToken: req.params.token,
            resetPasswordExpires: {$gt: Date.now()}
        }
    };
    Admin.load(options, function (err, admin) {
        if (err) return next(new HttpError(500, err.message));
        if (!admin) {
            req.flash('error', 'Посилання для зкидання пароля невірне');
            return res.redirect('/admin/forgot');
        }

        res.sendDataClient({
            email: admin.email
        }, 'admin/reset');

    });
};

exports.changePassword = function (req, res, next) {
    const options = {
        criteria: {
            resetPasswordToken: req.params.token,
            resetPasswordExpires: {$gt: Date.now()}
        }
    };
    Admin.load(options, function (err, admin) {
        if (err) return next(new HttpError(500, err.message));
        if (!admin) {
            req.flash('error', 'Посилання для зкидання пароля невірне');
            return res.redirect('/admin/forgot');
        }

        admin.password = req.body.password;
        admin.resetPasswordToken = undefined;
        admin.resetPasswordExpires = undefined;

        admin.save(function (err) {
            if (err) {
                const errors = err.errors;
                let messages = '';
                for (let item in errors) {
                    if (!errors.hasOwnProperty(item)) continue;
                    messages += errors[item].message + '\n';
                }
                req.flash('error', messages);
                return res.redirect(req.url);
            }

            req.logIn(admin, function (err) {
                if (err) {
                    req.flash('error', 'Невдалося увійти');
                    return res.redirect('/admin/login');
                }
                res.redirect('/admin');
            });

            const transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'museum995192@gmail.com',
                    pass: 'ctrhtnysqgfhjkm'
                }
            });

            var mailOptions = {
                from: 'museum995192@gmail.com', // sender address
                to: admin.email,
                subject: 'Пароль змінений ✔',
                html: '<p>Пароль був успішно змінений</p>'
            };

            transporter.sendMail(mailOptions, function (err, info) {
            });

        });

    });
};

exports.profile = function (req, res, next) {
    Message.list({criteria: {view: false}}, true, function (err, countMsg) {
        if (err) {
            countMsg = '?';
        }
        const objSend = {
            countMsg,
            title: 'Профіль',
            url: req.originalUrl,
            admin: req.admin,
            error: req.flash('error'),
            success: req.flash('success'),
            isRoot: req.isRoot
        };

        if (req.isRoot) {
            Admin.list({adminname: {$ne: 'root'}}, function (err, list) {
                if (err) return next(new HttpError(500, err.message));
                objSend.admins = list;

                res.sendDataClient(objSend, 'admin/profile');
            })
        } else {
            res.sendDataClient(objSend, 'admin/profile');
        }

    });
};

exports.changeProfile = function (req, res, next) {
    const admin = req.admin;
    let url = req.originalUrl;
    if (url.indexOf('?') + 1) {
        url = url.slice(0, url.indexOf('?'));
    }
    const body = req.body;

    if (body.password == body.confirm) {
        if (req.user.authenticate(body.passwordOld)) {
            if (body.password) {
                admin.password = body.password;
            }
            if (body.email) admin.email = body.email;
            admin.save(function (err) {
                if (err) {
                    const errors = err.errors;
                    let messages = '';
                    for (let item in errors) {
                        if (!errors.hasOwnProperty(item)) continue;
                        messages += errors[item].message + '\n';
                    }
                    req.flash('error', messages);
                    return res.redirect(url);
                }
                req.logIn(admin, function (err) {
                    if (err) {
                        req.flash('error', 'Невдалося увійти');
                        return res.redirect(url);
                    }
                    req.flash('success', 'Інформація змінена');
                    return res.redirect(url);
                })
            });
        } else {
            req.flash('error', 'Пароль не вірний');
            return res.redirect(url);
        }
    } else {
        req.flash('error', 'Паролі не співпадаються');
        return res.redirect(url);
    }
};


exports.destroy = function (req, res, next) {
    const id = req.params.idAdmDel;
    const options = {
        criteria: {_id: id}
    };
    Admin.load(options, function (err, admin) {
        if (err) return next(new HttpError(500, err.message));
        if (!admin) {
            return next(new Error('Адміністратор не знайдений'));
        }
        admin.remove(function (err) {
            if (err) {
                req.flash('error', err.message);
            } else {
                req.flash('success', 'Адміністратор видалений');
            }
            let url = req.originalUrl;
            url = url.slice(0, url.lastIndexOf('/'));

            return res.redirect(url);
        })
    });
};