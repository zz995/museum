'use strict';

const mongoose = require('mongoose');
const ObjectID = mongoose.Types.ObjectId;
const Exhibit = mongoose.model('Exhibit');
const Category = mongoose.model('Category');
const imageManager = require('../models/imageManager.js');
const fs = require('fs');
const path = require('path');
const HttpError = require('../libs/error').HttpError;
const paramSelect = require('../libs/paramSelect');
const config = require('../config');
const Message = mongoose.model('Message');
const log = require('../libs/log').getLogger(module);
const only = require('only');
const escape = require('escape-string-regexp');

const handleErrorDB = function (success, msg, req) {
    (new Message({
            status: success,
            message: msg,
            section: 'Експонати',
            user: req.user.adminname,
            url: req.method + ' ' + req.originalUrl
        })
    ).save(function (err) {
            if (err) {
                log.error(err);
            }
        });
};

const delAllFile = function (allFile, folder, req) {
    Exhibit.destroyFile(allFile, function (msgErr) {
        if (folder) {
            fs.rmdir(folder, function (err) {
                if (err) {
                    msgErr.message += msgErr.message + '\n';
                } else {
                    msgErr.message += 'successfully deleted folder ' + folder + '\n';
                }
                handleErrorDB(msgErr.success, msgErr.message, req);
            });
        } else {
            handleErrorDB(msgErr.success, msgErr.message, req);
        }
    });
};

const getCatArray = function (docs) {
    let arrayCats = {};

    for (let i = 0; i < docs.length; i++) {
        let doc = docs[i];
        let par = doc.parent === null ? '0' : doc.parent;
        arrayCats[par] = arrayCats[par] || {};
        arrayCats[par][doc._id] = {
            _id: doc._id.toString(),
            name: doc.name
        }
    }

    return arrayCats;
};

const getAllFile = function (req) {
    const listFile = [];

    if (req.files) {
        let img = req.files.images;
        let doc = req.files.docs;
        let mainImg = req.files.mainPicture;

        let model3dBin = req.files.model3dBin;
        let model3dJson = req.files.model3dJson;
        let altModel3d = req.files.altModel3d;
        if (img) {
            for (let i = 0; i < img.length; i++) {
                img[i].path && listFile.push(img[i].path);
            }
        }
        if (doc) {
            for (let i = 0; i < doc.length; i++) {
                doc[i].path && listFile.push(doc[i].path);
            }
        }
        mainImg && mainImg[0].path && listFile.push(mainImg[0].path);
        model3dBin && model3dBin[0].path && listFile.push(model3dBin[0].path);
        model3dJson && model3dJson[0].path && listFile.push(model3dJson[0].path);
        altModel3d && altModel3d[0].path && listFile.push(altModel3d[0].path);
    }

    req.currentFileUpload && listFile.push(req.currentFileUpload);

    return listFile;
};

const getFolder3d = function (req) {
    if (req.files) {
        let model3dBin = req.files.model3dBin;
        let model3dJson = req.files.model3dJson;

        if (model3dBin && model3dBin[0].path) return path.dirname(model3dBin[0].path);
        if (model3dJson && model3dJson[0].path) return path.dirname(model3dJson[0].path);
    }
    if (req.currentFileUpload) {
        if (req.currentFileUploadIs3d) {
            return path.dirname(req.currentFileUpload);
        }
    }
    return null;
};

const transformListCat = function (arrayCats, allFile, folder, req) {
    let arrStCat = arrayCats.split(', ');
    const arrObCats = [];
    for (let l = arrStCat.length; l--;) {
        let ob;
        try {
            ob = new ObjectID(arrStCat[l]);
        } catch (e) {
            handleErrorDB(false, e.message, req);
            delAllFile(allFile, folder, req);
            return null;
        }
        arrObCats.push(ob);
    }
    return arrObCats;
};

exports.load = function (req, res, next, id) {
    try {
        id = new ObjectID(req.params.id);
    } catch (e) {
        return next(400);
    }
    Exhibit.load(id, function (err, exhibit) {
        if (err) {
            return next(new HttpError(500, 'Невдалося завантажити екземпляр'));
        }
        if (!exhibit) {
            return next(404);
        }

        req.exhibit = exhibit;
        next();
    });
};

exports.setOptions = function (req, res, next) {
    const criteria = req.criteria || {};

    if (req.query.search) {
        criteria.title = {$regex: new RegExp(escape(req.query.search), 'i')};
    }
    if (!!req.query.chname) {
        const prop = {};
        criteria.characteristics = {$elemMatch: {}};
        prop.name = req.query.chname;
        if (!!req.query.chvalue) {
            prop.value = req.query.chvalue;
        }
        criteria.characteristics = {$elemMatch: prop};
    }

    req.opt = paramSelect({req: req.query, criteria});

    next();
};

exports.list = function (req, res, next) {
    req.opt.criteria.hidden = false;
    Exhibit.list(req.opt, false, function (err, exhibits) {
        if (err) {
            return next(err);
        }
        Exhibit.list(req.opt, true, function (err, count) {
            if (err) {
                return next(err);
            }
            res.sendDataClient({count, exhibits});
        })
    })
};

exports.index = function (req, res, next) {
    if (req.query.hidden === 'true' || req.query.hidden === 'false') {
        req.opt.criteria.hidden = req.query.hidden == 'true';
    }
    req.opt.isAdmin = true;
    Exhibit.list(req.opt, false, function (err, exhibits) {
        if (err) {
            return next(err);
        }
        Exhibit.list(req.opt, true, function (err, count) {
            if (err) {
                return next(err);
            }
            Category.list({}, function (err, docs) {
                if (err) {
                    return next(err);
                }
                Message.list({criteria: {view: false}}, true, function (err, countMsg) {
                    if (err) {
                        countMsg = '?';
                    }

                    const arrayCats = getCatArray(docs);
                    arrayCats.isShow = true;

                    const options = {};

                    const query = req.query;
                    Object.assign(options, only(req.opt, 'limit page'), only(query, 'search chname chvalue hidden'));
                    options.sortString = query.sort || 'createdAt_-1';
                    options.sort = req.opt.sort;

                    const dataForSend = {
                        title: 'Список експонатів',
                        pages: Math.ceil(count / options.limit),
                        count, exhibits,
                        admin: req.user,
                        tag: req.params.tag,
                        countMsg,
                        options,
                        arrayCats
                    };

                    if (req.category) {
                        dataForSend.category = {
                            name: req.category.name,
                            id: req.category.id
                        }
                    }

                    res.sendDataClient(dataForSend, 'exhibits');
                });
            });
        });
    });
};

exports.uploadAndParse = function (req, res, next) {
    const closeHandle = () => {
        log.error('З\'єднання було розірване');
        req.outStream.destroy();
        delAllFile(getAllFile(req), getFolder3d(req), req);
        res.redirect('/admin/exhibit');
    };

    res.once('close', closeHandle);

    Exhibit.upload(req, res, function (err) {
        res.removeListener('close', closeHandle);
        req.currentFileUpload = null;
        if (err) {
            handleErrorDB(false, err.message, req);
            delAllFile(getAllFile(req), getFolder3d(req), req);
            res.redirect('/admin/exhibit');
        } else {
            if (req.files.model3dBin || req.files.model3dJson) {
                if (req.files.model3dBin && req.files.model3dJson) {
                    res.redirect('/admin/exhibit');
                    return next();
                } else {
                    handleErrorDB(false, '3d модель повинна складатися з двох файлів: .JSON і .BIN', req);
                    delAllFile(getAllFile(req), getFolder3d(req), req);
                    res.redirect('/admin/exhibit');
                }
            } else {
                res.redirect('/admin/exhibit');
                return next();
            }
        }
    });
};

exports.create = function (req, res, next) {
    let img = req.files.images;
    let doc = req.files.docs;
    let mainImg = req.files.mainPicture;
    let model3dBin = req.files.model3dBin;
    let model3dJson = req.files.model3dJson;
    let altModel3d = req.files.altModel3d;

    req.body.categories = transformListCat(req.body.arrayCats, getAllFile(req), getFolder3d(req), req);
    if (!req.body.categories) return next(400);

    const ex = {};
    Object.assign(ex, only(req.body, 'hidden name inventoryNumber museumNumber dateStarted dateFinish ' +
        'tags characteristics mediaCDN bodyShort body categories'));
    const exhibit = new Exhibit(ex);

    if (img) exhibit.images = img;
    if (doc) exhibit.docs = doc;
    if (mainImg) exhibit.img = mainImg[0];

    if (model3dBin && model3dJson) {
        exhibit.model3d.bin = model3dBin[0].relativepath;
        exhibit.model3d.json = model3dJson[0].relativepath;
    }
    if (altModel3d) {
        exhibit.altModel3d.relativepath = altModel3d[0].relativepath;
        exhibit.altModel3d.count = req.body.countFrame || 12;
    }

    const images = exhibit.images;
    const desImg = req.body.descripImg;

    for (let i = 0; i < images.length && desImg; i++) {
        images[i].description = desImg[images[i].originalname];
    }

    const docs = exhibit.docs;
    const desDoc = req.body.descripDoc;
    for (let i = 0; i < docs.length; i++) {
        docs[i].description = desDoc[docs[i].originalname];
    }

    const error = exhibit.validateSync();
    if (error) {
        handleErrorDB(false, error.message, req);
        return delAllFile(getAllFile(req), getFolder3d(req), req);
    }

    req.exhibit = exhibit;
    next();
};

exports.resize = function (req, res, next) {
    if (!req.files.mainPicture && !req.files.images) {
        return next();
    }

    Exhibit.resize(req, function (msg) {
        if (msg.resizeErr) {
            handleErrorDB(false, msg.resizeErr, req);
        }
        handleErrorDB(msg.unlinkMsg.success, msg.unlinkMsg.message, req);

        return next();
    });
};

exports.save = function (req, res, next) {
    const exhibit = req.exhibit;
    exhibit.saveExhibit(function (err, errTagUpdate) {
        if (err) {
            handleErrorDB(false, err.message, req);
            exhibit.destroyFile(null, function (err, msg) {
                handleErrorDB(!err, msg, req);
            })
        }
        if (errTagUpdate) {
            handleErrorDB(false, 'Не вдалося обновити список тегів', req);
        }

        handleErrorDB(true, req.messageClientExhibit || 'Успішно', req);
    });
};

exports.showforapi = function (req, res, next) {
    if (req.exhibit.hidden) {
        next(404);
    } else {
        res.sendDataClient({
            exhibit: req.exhibit
        }, 'exhibits/show');
    }
};

exports.show = function (req, res, next) {
    res.sendDataClient({
        exhibit: req.exhibit
    }, 'exhibits/show');
};

exports.new = function (req, res, next) {
    Category.list({}, function (err, docs) {
        if (err) {
            return next(err);
        }

        Message.list({criteria: {view: false}}, true, function (err, countMsg) {
            if (err) {
                countMsg = '?';
            }

            const pathf = path.join(__dirname, '../public', config.get('globalImageRoot'));
            imageManager.index(pathf, function (err, list) {
                if (err) {
                    if (err.code == 'ENOENT') {
                        return next(404);
                    }
                    return next(err);
                }

                res.sendDataClient({
                    admin: req.user,
                    isNew: true,
                    title: 'Новий експонат',
                    countMsg,
                    exhibit: new Exhibit({}),
                    arrayCats: getCatArray(docs),
                    imageManager: {dir: '/', list, path: config.get('globalImageRoot')}
                }, 'exhibits/new');
            });

        });
    });
};

exports.edit = function (req, res, next) {
    Category.list({}, function (err, docs) {
        if (err) {
            return next(err);
        }
        Message.list({criteria: {view: false}}, true, function (err, countMsg) {
            if (err) {
                countMsg = '?';
            }
            const pathf = path.join(__dirname, '../public', config.get('globalImageRoot'));
            imageManager.index(pathf, function (err, list) {
                if (err) {
                    if (err.code == 'ENOENT') {
                        return next(404);
                    }
                    return next(err);
                }

                const cats = req.exhibit.categories;
                const cid = [];
                const cname = [];
                for (let i = 0; i < cats.length; i++) {
                    cid.push(cats[i]._id);
                    cname.push(cats[i].name);
                }
                res.sendDataClient({
                    admin: req.user,
                    isNew: false,
                    title: 'Редагування експоната',
                    countMsg,
                    exhibit: req.exhibit,
                    categoryId: cid.join(', '),
                    categoryName: cname.join(', '),
                    transformSize: config.get('upload:img:transformSize'),
                    arrayCats: getCatArray(docs),
                    imageManager: {dir: '/', list, path: config.get('globalImageRoot')}
                }, 'exhibits/edit');
            });
        })
    })
};

exports.update = function (req, res, next) {

    const exhibit = req.exhibit;

    let images = req.files.images;
    let docs = req.files.docs;
    let img = req.files.mainPicture;
    let model3dBin = req.files.model3dBin;
    let model3dJson = req.files.model3dJson;
    let altModel3d = req.files.altModel3d;

    const prepareObj = exhibit.prepareDelete({
        delImg: req.body.deleteImg,
        delDoc: req.body.deleteDoc,
        img, model3dJson, model3dBin, altModel3d,
        model3dDelete: !!req.body.model3dDelete,
        altModel3dDelete: !!req.body.altModel3dDelete
    });

    req.body.categories = transformListCat(req.body.arrayCats, getAllFile(req), getFolder3d(req), req);
    if (!req.body.categories) return next(404);

    Object.assign(exhibit, only(req.body, 'name inventoryNumber museumNumber dateStarted dateFinish ' +
        'tags characteristics mediaCDN bodyShort body categories'));
    exhibit.hidden = !!req.body.hidden;

    const stayImgDB = prepareObj.stayDB.images;
    if (stayImgDB) {
        const newImg = [];
        const oldImg = exhibit.images;
        for (let i = 0, l = stayImgDB.length; i < l; i++) {
            newImg.push(oldImg[stayImgDB[i]]);
        }
        exhibit.images = newImg;
    }

    const stayDocDB = prepareObj.stayDB.docs;
    if (stayDocDB) {
        const newDoc = [];
        const oldDoc = exhibit.docs;
        for (let i = 0, l = stayDocDB.length; i < l; i++) {
            newDoc.push(oldDoc[stayDocDB[i]]);
        }
        exhibit.docs = newDoc;
    }

    const lengthOldImgs = exhibit.images.length;
    const lengthOldDocs = exhibit.docs.length;

    if (images) exhibit.images = exhibit.images.concat(images);
    if (docs) exhibit.docs = exhibit.docs.concat(docs);
    if (img) exhibit.img = img[0];

    if (model3dBin && model3dJson) {
        exhibit.model3d.bin = model3dBin[0].relativepath;
        exhibit.model3d.json = model3dJson[0].relativepath;
    } else if (!!req.body.model3dDelete) {
        exhibit.model3d = undefined;
    }

    if (req.body.countFrame || altModel3d) exhibit.altModel3d.count = req.body.countFrame || 12;

    if (altModel3d) {
        exhibit.altModel3d.relativepath = altModel3d[0].relativepath;
    } else if (!!req.body.altModel3dDelete) {
        exhibit.altModel3d = undefined;
    }

    const desImg = req.body.descripImg;
    const changeDesImg = req.body.descripImgSaved;

    for (let i = 0, imgs = exhibit.images; i < imgs.length; i++) {
        imgs[i].description = i < lengthOldImgs
            ? changeDesImg && changeDesImg[path.basename(imgs[i].relativepath)]
            : desImg && desImg[imgs[i].originalname];
    }

    const desDoc = req.body.descripDoc;
    const changeDesDoc = req.body.descripDocSaved;

    for (let i = 0, docsEx = exhibit.docs; i < docsEx.length; i++) {
        docsEx[i].description = i < lengthOldDocs
            ? changeDesDoc && changeDesDoc[path.basename(docsEx[i].relativepath)]
            : desDoc && desDoc[docsEx[i].originalname];
    }

    const error = exhibit.validateSync();

    if (error) {
        handleErrorDB(false, error.message, req);
        return delAllFile(getAllFile(req), getFolder3d(req), req);
    }

    if (prepareObj.listFile.length) {
        exhibit.destroyFile(prepareObj, function (err, msg) {
            handleErrorDB(!err, msg, req);
            if (err) return delAllFile(getAllFile(req), getFolder3d(req), req);
            next();
        });
    } else {
        next();
    }
};

exports.destroy = function (req, res, next) {
    res.redirect('/admin/exhibit');
    const exhibit = req.exhibit;
    exhibit.destroyFile(null, function (err, msg) {
        handleErrorDB(!err, msg, req);
        if (err) return null;
        req.exhibit.remove(function (err) {
            if (err) return handleErrorDB(false, err.message, req);
            handleErrorDB(true, 'Експонат видаленний', req);

            Exhibit.tags(function (err) {
                if (err) handleErrorDB(false, 'Не вдалося обновити список тегів', req);
                return null;
            });
        });
    });
};