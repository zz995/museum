'use strict';

const mongoose = require('mongoose');
const ObjectID = require('mongodb').ObjectID;
const Category = mongoose.model('Category');
const Exhibit = mongoose.model('Exhibit');
const log = require('../libs/log').getLogger(module);
const HttpError = require('../libs/error').HttpError;
const paramSelect = require('../libs/paramSelect');
const Message = mongoose.model('Message');
const config = require('../config');
const path = require('path');

const handleErrorDB = function (success, msg, req) {
    (new Message({
            status: success,
            message: msg,
            section: 'Категорії',
            user: req.user.adminname,
            url: req.method + ' ' + req.originalUrl
        })
    ).save(err => {
            if (err) log.error(err);
        }
    );
};

const delAllImg = function (status, allImg, req, next) {
    status = status || 400;
    Category.destroyImg(allImg, function (msgErr) {
        handleErrorDB(msgErr.success, msgErr.message, req);
        return next(status);
    });
};

exports.load = function (req, res, next, id) {
    try {
        id = new ObjectID(req.params.idc);
    } catch (err) {
        return next(400);
    }
    Category.load(id, function (err, category) {
        if (err) return next(err);
        if (!category) {
            return next(404);
        }
        req.category = category;
        return next();
    });
};

exports.uploadAndParse = function (req, res, next) {
    const getFiles = () => {
        const listFile = [];
        req.currentFileUpload && listFile.push(req.currentFileUpload);
        return listFile;
    };

    const closeHandle = () => {
        log.error('З\'єднання було розірване');
        const listFiles = getFiles();
        req.outStream.destroy();
        if (listFiles.length) {
            Category.destroyImg(listFiles, function (e) {
                handleErrorDB(false, e.message, req);
            })
        }
    };
    res.once('close', closeHandle);

    Category.upload(req, res, function (err) {
        res.removeListener('close', closeHandle);
        if (err) {
            handleErrorDB(false, err.message, req);
            const listFiles = getFiles();
            let errorCode = 500;
            if (err.code) {
                if (err.code == 'LIMIT_FILE_SIZE') errorCode = 413;
                else if (err.code == 'UNSUPPORTED_MEDIA_TYPE') errorCode = 415;
            }
            if (listFiles.length) return delAllImg(errorCode, listFiles, req, next);
            return next(errorCode);
        } else {
            next();
        }
    });
};

exports.create = function (req, res, next) {
    if (!req.file) return next(400);

    const category = new Category();
    const allImg = [];
    allImg.push(req.file.path);
    category.title = req.body.title;
    if (req.body.charact) category.charact = req.body.charact;
    category.body = req.body.body;
    category.img.path = req.file.relativepath;
    category.img.extname = req.file.extname;

    if (req.body.id === '0') {
        category.parent = null;
    } else {
        try {
            category.parent = new ObjectID(req.body.id);
        } catch (err) {
            handleErrorDB(false, err.message, req);
            delAllImg(400, allImg, req, next);
        }
    }

    const err = category.validateSync();
    if (err) {
        handleErrorDB(false, 'Validate error', req);
        delAllImg(400, allImg, req, next);
    } else {
        req.category = category;
        next();
    }
};

exports.resize = function (req, res, next) {
    if (!req.file) return next();

    Category.resize(req, function (msg) {
        if (msg && msg.resizeErr) {
            handleErrorDB(false, msg.resizeErr, req);
            handleErrorDB(msg.unlinkMsg.success, msg.unlinkMsg.message, req);
            req.messageClientCategory = 'Невдалося завантажити зображення';
            return next();
        }

        req.messageClientCategory = 'Успішно';
        return next();
    });
};

exports.save = function (req, res, next) {
    const category = req.category;
    const saveError = function (err) {
        handleErrorDB(false, err.message, req);
        const listFile = [];
        const img = category.img.path;
        const transformSize = config.get('categoryImg:transformSize');

        listFile.push(path.join(__dirname, '../public', img + category.img.extname));
        for (let i = 0, l = transformSize.length; i < l; i++) {
            listFile.push(path.join(__dirname, '../public', `${img}_${transformSize[i].pre}.jpg`));
        }

        delAllImg(err.code == 'CONFLICT' ? 409 : 500, listFile, req, next);
    };
    category.saveCategoryUpdateParent(function (err) {
        if (err) return saveError(err);
        res.sendDataClient({
            message: req.messageClientCategory || 'Успішно',
            id: category._id,
            name: category.name
        });
    });
};

exports.showCategory = function (req, res, next) {
    const opt = ['body', 'img', 'charact'];
    const optOb = {};

    for (let i = 0; i < opt.length; i++) {
        if (req.query[opt[i]] === '1') {
            optOb[opt[i]] = 1;
        }
    }

    Category.list(optOb, function (err, category) {
        if (err) return next(err);
        res.sendDataClient(category);
    });
};

exports.deleteImg = function (req, res, next) {
    if (req.file) {
        req.category.destroyImg(function (err) {
            if (err) {
                const allImg = [];

                allImg.push(req.file.path);
                delAllImg(500, allImg, req, next);
            } else {
                return next();
            }
        })
    } else {
        next();
    }
};

exports.update = function (req, res, next) {
    const category = req.category;
    category.isUpdate = true;
    req.messageClientCategory = req.messageClientCategory || 'Успішно';
    if (req.body.title) category.title = req.body.title;
    if (req.body.charact) category.charact = req.body.charact;
    if (req.body.body) category.body = req.body.body;
    if (req.file) {
        category.img.path = req.file.relativepath;
        category.img.extname = req.file.extname;
    }

    next();
};

exports.index = function (req, res, next) {
    const childs = req.category.childs;
    const listId = [req.category._id];
    for (let i = 0, l = childs.length; i < l; i++) {
        listId.push(childs[i]);
    }

    req.criteria = {
        categories: {
            $in: listId
        }
    };

    next();
};

exports.destroy = function (req, res, next) {
    let category = req.category;

    const handleErr = err => {
        handleErrorDB(false, err.message, req);
        next(err.code == 'CONFLICT' ? 409 : 500);
    };

    Category.leaf(category._id, function (err, bool) {
        if (err) return handleErr(err);
        if (bool) {
            Exhibit.used(category._id.toString(), function (err, check) {
                if (err) return handleErr(err);
                if (check) {
                    category.destroyImg(function (err, msg) {
                        handleErrorDB(!err, msg, req);
                        if (err) {
                            return next(new HttpError(500, 'Невдалося видалити зображення які належать даній категорії', req));
                        } else {
                            category.deleteCategoryUpdateParent(function (err) {
                                if (err) return handleErr(err);
                                res.sendDataClient({
                                    message: 'Категорія була видалена'
                                });
                            });
                        }
                    })

                } else {
                    handleErrorDB(false, 'Неможливо видалалити категорію, вона міститься в експонатах', req);
                    return next(400);
                }
            });
        } else {
            handleErrorDB(false, 'Неможливо видалалити категорію, вона не є листком', req);
            return next(400);
        }
    });
};

exports.info = function (req, res, next) {
    const cat = req.category;
    res.sendDataClient({body: cat.body, charact: cat.charact, img: cat.img, name: cat.name});
};

exports.move = function (req, res, next) {
    const category = req.category;

    if (category.museumInfo && req.body.id !== '0') {
        return res.sendDataClient(new HttpError(400, 'Категорія, яка виступая в якості музею не може бути підкатегорією'));
    }

    category.isUpdate = true;
    req.messageClientCategory = req.messageClientCategory || 'Успішно';
    category.oldIdPar = category.parent;

    if (req.body.id === '0') {
        category.parent = null;
    } else {
        try {
            category.parent = new ObjectID(req.body.id);
        } catch (err) {
            handleErrorDB(false, err.message, req);
            return next(new HttpError(400, err.message));
        }
    }

    category.move(function (err) {
        if (err) {
            handleErrorDB(false, err.message, req);
            next(err.code == 'CONFLICT' ? 409 : 500);
        } else {
            res.sendDataClient({
                message: req.messageClientCategory || 'Успішно',
                id: category._id,
                name: category.name
            });
        }
    });
};

