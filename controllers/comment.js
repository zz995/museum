'use strict';

const mongoose = require('mongoose');
const Comment = mongoose.model('Comment');
const Message = mongoose.model('Message');
const ObjectID = mongoose.Types.ObjectId;
const only = require('only');
const HttpError = require('../libs/error').HttpError;
const paramSelect = require('../libs/paramSelect');

exports.load = function (req, res, next, id) {
    try {
        id = new ObjectID(req.params.idcom);
    } catch (e) {
        return next(400);
    }
    Comment.load(id, function (err, comment) {
        if (err) {
            return next(new HttpError(500, 'Невдалося завантажити коментарій'));
        }
        if (!comment) {
            return next(404);
        }

        req.comment = comment;
        next();
    });
};

exports.index = function (req, res, next) {
    const criteria = {};
    const visible = req.query.visible;
    if (visible !== undefined) {
        criteria.visible = visible == 'true';
    }
    const accessSort = [
        "createdAt_1", "createdAt_-1"
    ];
    const options = paramSelect({req: req.query, criteria, accessSort});
    options.sortString = req.query.sort || 'createdAt_-1';
    Message.list({criteria: {view: false}}, true, function (err, countMsg) {
        if (err) {
            countMsg = '?';
        }
        Comment.list(options, false, true, function (err, comments) {
            if (err) {
                return next(err);
            }
            Comment.list(options, true, true, function (err, count) {
                if (err) {
                    return next(err);
                }
                res.sendDataClient({
                    admin: req.user,
                    title: 'Коментарі',
                    countMsg,
                    options,
                    pages: Math.ceil(count / options.limit),
                    count, comments
                }, 'admin/comments');
            });
        });
    });

};

exports.list = function (req, res, next) {
    const options = paramSelect({req: req.query, criteria: {visible: true}, accessSort: []});
    Comment.list(options, false, false, function (err, comments) {
        if (err) {
            return next(err);
        }
        Comment.list(options, true, false, function (err, count) {
            if (err) {
                return next(err);
            }
            res.sendDataClient({count, comments});
        });
    });
};

exports.create = function (req, res, next) {
    if (!req.body.body || !req.body.author) {
        return next(400);
    }

    const comment = new Comment(Object.assign({}, only(req.body, 'author body')));

    comment.saveComment(function (err) {
        if (err) {
            return next(new HttpError(400, err.msg));
        }

        res.sendDataClient({
            message: 'Коментарій доданий'
        });
    });
};

exports.update = function (req, res, next) {
    const comment = req.comment;
    req.body.visible = !!req.body.visible;
    Object.assign(comment, only(req.body, 'author body visible'));
    comment.saveComment(function (err) {
        if (err) {
            return next(new HttpError(400, err.msg));
        }
        res.redirect('/admin/comment');
    });
};

exports.destroy = function (req, res, next) {
    req.comment.remove(function (err) {
        if (err) {
            return next(new HttpError(500, err.msg));
        }
        res.redirect('/admin/comment');
    });
};