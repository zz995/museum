'use strict';

const mongoose = require('mongoose');
const log = require('../libs/log').getLogger(module);
const HttpError = require('../libs/error').HttpError;
const paramSelect = require('../libs/paramSelect');
const config = require('../config');

exports.index = function (req, res, next) {
    const tags = req.params.tag;
    const accessSort = [
        "titleIntl_1", "titleIntl_-1",
        "createdAt_1", "createdAt_-1"
    ];
    const criteria = {tags};

    const opt = paramSelect({req: req.query, accessSort, criteria});

    req.opt = opt;
    next();
};

exports.list = function (req, res, next) {
    let tags = global.tags;
    if (!tags) return next(500);
    const limit = +req.query.limit;
    if (limit > 1 && limit < config.get('tags:maxCount')) tags = tags.slice(0, limit);
    res.sendDataClient({tags});
};