'use strict';

const mongoose = require('mongoose');
const log = require('../libs/log').getLogger(module);
const HttpError = require('../libs/error').HttpError;
const Message = mongoose.model('Message');
const monitoring = require('../models/monitoring');
var config = require('../config');
const os = require('os');

exports.index = function (req, res, next) {
    monitoring.info(function (err, db) {
        if (err) return next(500);
        Message.list({criteria: {view: false}}, true, function (err, countMsg) {
            if (err) {
                countMsg = '?';
            }
            res.render('admin/monitoring', {
                countMsg,
                title: 'Моніторинг',
                admin: req.user,
                db,
                config: config.get(),
                os: {
                    type: os.type(),
                    arch: os.arch(),
                    release: os.release(),
                    cpus: os.cpus(),
                    freemem: os.freemem(),
                    totalmem: os.totalmem(),
                    hostname: os.hostname(),
                    networkInterfaces: os.networkInterfaces()
                }
            });
        });
    });
};