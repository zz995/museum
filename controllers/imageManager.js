'use strict';

const imageManager = require('../models/imageManager.js');
const HttpError = require('../libs/error').HttpError;
const path = require('path');
const config = require('../config');
const log = require('../libs/log').getLogger(module);

exports.index = function (req, res, next) {
    let pathReq = path.normalize(req.query.path || '\\');
    const pathf = path.join(__dirname, '../public', config.get('globalImageRoot'), pathReq);

    imageManager.index(pathf, function (err, list) {
        if (err) {
            if (err.code == 'ENOENT') {
                return next(404);
            }
            return next(err);
        }

        res.sendDataClient({dir: pathReq, list});
    })
};

exports.destroy = function (req, res, next) {
    let pathReq = path.normalize(req.body.path.toString());

    if (pathReq == path.sep) {
        return next(400);
    }

    const pathf = path.join(__dirname, '../public', config.get('globalImageRoot'), pathReq);

    imageManager.destroy(pathf, function (err) {
        if (err) {
            return next(err);
        }

        res.sendDataClient({
            message: 'Файл або папка видаленні'
        });
    })
};

exports.rename = function (req, res, next) {
    let pathReq = path.normalize(req.body.path.toString());
    let name = path.normalize(req.body.name.toString());

    if (pathReq == path.sep) {
        return next(400);
    }

    const pathf = path.join(__dirname, '../public', config.get('globalImageRoot'), pathReq);

    imageManager.rename(pathf, name, function (err) {
        if (err) {
            if (err.code == 'ENOENT') {
                return next(new HttpError(404, 'Шлях не знайдений'));
            }
            if (err.code == 'EPERM') {
                return next(new HttpError(400, 'В даній папці файл або папка вже існує з таким іменем'));
            }

            return next(err);
        }

        res.sendDataClient({
            message: 'Файл або папка переіменованна'
        });
    })
};

exports.move = function (req, res, next) {
    let pathReq = path.normalize(req.body.path.toString());
    let newPath = path.normalize(req.body.newPath || '\\');

    if (!newPath.indexOf(pathReq)) return next(new HttpError(400, 'Не можливо перемістити'));
    if (pathReq == path.sep) return next(400);

    const pathf = path.join(__dirname, '../public', config.get('globalImageRoot'), pathReq);
    const pathn = path.join(__dirname, '../public', config.get('globalImageRoot'), newPath);

    imageManager.move(pathf, pathn, function (err) {
        if (err) {
            if (err.code == 'ENOENT') {
                return next(new HttpError(404, 'Шлях не знайдений'));
            }
            if (err.code == 'EPERM') {
                return next(new HttpError(400, 'В даній папці файл або папка вже існує з таким іменем'));
            }

            return next(err);
        }

        res.sendDataClient({
            message: 'Файл або папка переміщенна'
        });
    })
};

exports.dir = function (req, res, next) {
    let pathReq = path.normalize(req.body.path || '\\');
    let name = path.normalize(req.body.name.toString());

    const pathf = path.join(__dirname, '../public', config.get('globalImageRoot'), pathReq);

    imageManager.createDir(pathf, name, function (err) {
        if (err) {
            if (err.code == 'ENOENT') {
                return next(new HttpError(404, 'Шлях не знайдений'));
            }
            if (err.code == 'EEXIST') {
                return next(new HttpError(400, 'В даній папці файл або папка вже існує з таким іменем'));
            }

            return next(err);
        }

        res.sendDataClient({
            message: 'Папка створенна'
        });
    })
};

exports.imgUpload = function (req, res, next) {

    const closeHandle = () => {
        imageManager.destroy(req.currentFileUpload, function (err) {
            if (err) log.error(err);
        });
    };

    res.once('close', closeHandle);

    imageManager.uploadImg(req, res, function (err) {
        res.removeListener('close', closeHandle);
        if (err) {
            return next(new HttpError(400, err.message));
        }

        let pathTem = req.file.path;
        let newPath = path.normalize(req.body.path || '\\');
        newPath = path.join(__dirname, '../public', config.get('globalImageRoot'), newPath);

        imageManager.move(pathTem, newPath, function (err) {
            if (err) {
                imageManager.destroy(pathTem, function (err) {
                    if (err) {
                        return next(new HttpError(400, 'Файл не вдалося видалити з тимчасової папки'));
                    }
                    return next(new HttpError(400, 'Файл не вдалося завантажити'));
                });
            } else {
                res.sendDataClient({
                    message: 'Файл завантажений'
                });
            }
        })
    })
};