'use strict';

const mongoose = require('mongoose');
const ObjectID = mongoose.Types.ObjectId;
const Message = mongoose.model('Message');
const log = require('../libs/log').getLogger(module);
const HttpError = require('../libs/error').HttpError;
const paramSelect = require('../libs/paramSelect');

exports.index = function (req, res, next) {
    const criteria = {};
    const view = req.query.view;

    if (view !== undefined) {
        criteria.view = view == 'true';
    }

    const accessSort = [
        "createdAt_1", "createdAt_-1"
    ];
    const options = paramSelect({req: req.query, criteria, accessSort});

    options.sortString = req.query.sort || 'createdAt_-1';

    Message.list(options, false, function (err, msg) {
        if (err) {
            return next(err);
        }

        Message.list(options, true, function (err, count) {
            if (err) {
                return next(err);
            }

            const ids = [];

            for (let l = msg.length; l--;) {
                ids.push(msg[l]._id);
            }
            if (view === undefined || view == 'false') {

                Message.updateMsg(ids, function (err) {
                    if (err) {
                        next(err);
                    } else {
                        res.sendDataClient({
                            admin: req.user,
                            title: 'Повідомлення',
                            options,
                            pages: Math.ceil(count / options.limit),
                            count, msg
                        }, 'exhibits/message');
                    }
                });
            } else {
                res.sendDataClient({
                    admin: req.user,
                    title: 'Повідомлення',
                    options,
                    pages: Math.ceil(count / options.limit),
                    count, msg
                }, 'exhibits/message');
            }

        })
    })
};

exports.destroy = function (req, res, next) {
    let ravIdm = [];

    const listDelMsg = req.body.del;
    for (let item in listDelMsg) {
        ravIdm.push(item);
    }

    let view = req.body.view == 'true';

    const criteria = {};

    if (view) {
        criteria.view = true;
    } else {
        if (ravIdm) {
            const idm = [];
            for (let l = ravIdm.length; l--;) {
                let ob;
                try {
                    ob = new ObjectID(ravIdm[l]);
                } catch (e) {
                    return next(400);
                }
                idm.push(ob);
            }

            criteria._id = {
                $in: idm
            }
        } else {
            return res.redirect('/admin/message');
        }
    }

    Message.destroy(criteria, function (err) {
        if (err) {
            return next(err);
        }
        return res.redirect('/admin/message');
    })
};
