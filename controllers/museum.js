'use strict';

const mongoose = require('mongoose');
const ObjectID = mongoose.Types.ObjectId;
const Exhibit = mongoose.model('Exhibit');
const Category = mongoose.model('Category');
const imageManager = require('../models/imageManager.js');
const fs = require('fs');
const path = require('path');
const removeFiles = require('../libs/removeFiles').remove;
const HttpError = require('../libs/error').HttpError;
const paramSelect = require('../libs/paramSelect');
const config = require('../config');
const Message = mongoose.model('Message');
const log = require('../libs/log').getLogger(module);
const only = require('only');

const getCatArray = function (docs) {
    let arrayCats = {};

    for (let i = 0; i < docs.length; i++) {
        let doc = docs[i];
        let par = doc.parent === null ? '0' : doc.parent;
        arrayCats[par] = arrayCats[par] || {};
        arrayCats[par][doc._id] = {
            _id: doc._id.toString(),
            name: doc.name
        }
    }

    return arrayCats;
};

exports.edit = function (req, res, next) {
    Category.list({}, function (err, docs) {
        if (err) {
            return next(err);
        }

        Message.list({criteria: {view: false}}, true, function (err, countMsg) {
            if (err) {
                countMsg = '?';
            }

            const pathf = path.join(__dirname, '../public', config.get('globalImageRoot'));
            imageManager.index(pathf, function (err, list) {
                if (err) {
                    if (err.code == 'ENOENT') {
                        return next(404);
                    }
                    return next(err);
                }

                res.sendDataClient({
                    title: 'Про музей',
                    catId: req.category._id,
                    countMsg,
                    admin: req.user,
                    museum: req.category.aboutMuseum,
                    arrayCats: getCatArray(docs),
                    imageManager: {dir: '/', list, path: config.get('globalImageRoot')},
                    error: req.flash('error'),
                    success: req.flash('success')
                }, 'museum/new');
            });

        });
    });
};

exports.update = function (req, res, next) {
    if (req.category.parent) {
        req.flash('success', 'Неможливо додати відомості до категорії');
        return res.redirect(req.originalUrl);
    }

    const sizeBody = req.body.body.length;

    if (sizeBody > 5000) {
        req.flash('error', `Перевищений допустимий ліміт символів в описанні, зараз: ${sizeBody}, потрібоно щоб було менше 5000`);
        return res.redirect(req.originalUrl);
    }
    req.category.museumInfo = true;

    const aboutMuseum = {contact: {}};
    Object.assign(aboutMuseum, only(req.body, 'name hidden body'));
    Object.assign(aboutMuseum.contact, only(req.body, 'index address phone email'));
    req.category.aboutMuseum = aboutMuseum;

    req.category.saveCategory(function (err) {
        if (err) {
            req.flash('error', err.message);
        } else {
            req.flash('success', 'Описання музею змінено');
        }
        res.redirect(req.originalUrl);
    });
};

exports.destroy = function (req, res, next) {
    req.category.aboutMuseum = undefined;
    req.category.museumInfo = false;
    req.category.saveCategory(function (err) {
        if (err) {
            req.flash('error', err.message);
        } else {
            req.flash('success', 'Описання видалено');
        }
        res.redirect(req.originalUrl.replace('_method=DELETE', '_method=PUT'));
    });
};

exports.info = function (req, res, next) {
    const museum = req.category.aboutMuseum;
    if (req.category.museumInfo && !museum.hidden) {
        if (req.query.description == "1")  return res.sendDataClient({name: museum.name, body: museum.body});
        if (req.query.contact == "1")  return res.sendDataClient(museum.contact);
        return res.sendDataClient(req.category.aboutMuseum);
    } else {
        next(404);
    }
};